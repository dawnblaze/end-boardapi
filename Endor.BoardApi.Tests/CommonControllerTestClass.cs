﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Newtonsoft.Json;
using Endor.DocumentStorage.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Endor.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
//using Microsoft.AspNetCore.Mvc.Internal;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Endor.Api.Common.Controllers;
using Endor.Api.Common.Services;
using Endor.Api.Common.Classes;

namespace Endor.BoardApi.Tests
{
    public class CommonControllerTestClass
    {
        protected const string SQLConfigName = "SQL";
        protected const string BlobStorageConfigName = "BlobStorage";

        protected const string TestProjectName = "Endor.BoardApi.Tests";
        protected const string TestTextFileName = "test.txt";
        protected const string TestTextFilePath = TestProjectName + "\\" + TestTextFileName;

        protected const string TestTextTwoFileName = "test_two.txt";
        protected const string TestTextTwoFilePath = TestProjectName + "\\" + TestTextTwoFileName;

        protected const string Test1x1PixelPngFileName = "1x1pixel.png";
        protected const string Test1x1PixelPngFilePath = TestProjectName + "\\" + Test1x1PixelPngFileName;
        protected const int AuthUserID = 1;
        protected const int UserLinkID = -98;

        protected IConfigurationRoot _config;

        [TestInitialize]
        public virtual void Init()
        {
            string file = "..\\..\\..\\client-secrets.json";
            if (!File.Exists(file))
            {
                File.WriteAllText(file, @"{
    ""SQL"": ""Data Source=.\\SQLEXPRESS;Initial Catalog=\""Dev.Endor.Business.DB1\"";User ID=cyrious;Password=watankahani"",
    ""BlobStorage"":  ""UseDevelopmentStorage=true""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);

            this._config = new ConfigurationBuilder().AddInMemoryCollection(result).Build();
            Assert.IsTrue(_config[SQLConfigName] != null && _config[SQLConfigName].Length > 0);
            Assert.IsTrue(_config[BlobStorageConfigName] != null && _config[BlobStorageConfigName].Length > 0);
        }

        protected async Task<M> AssertCreate<M, S, I>(CRUDController<M, S, I> controller, M toCreate)
            where M : class, IAtom<I>
            where S : AtomCRUDService<M, I>
            where I : struct, IConvertible
        {
            var result = await controller.Create(toCreate);
            var okPost = AssertX.IsType<OkObjectResult>(result);
            return AssertX.IsType<M>(okPost.Value);
        }

        protected static R AssertIsObjectResponseWithObject<T, R>(IActionResult response)
            where T : ObjectResult
        {
            var ok = AssertX.IsType<T>(response);
            Assert.IsNotNull(ok.Value);
            return AssertX.IsType<R>(ok.Value);
        }

        private MockTenantDataCache GetMockTenantDataCache()
        {
            return new MockTenantDataCache(_config[SQLConfigName], _config[BlobStorageConfigName]);
        }

        protected T GetAtomCRUDController<T, M, S, I>(short BID = TestConstants.BID)
            where T : CRUDController<M, S, I>
            where M : class, IAtom<I>
            where S : AtomCRUDService<M, I>
            where I : struct, IConvertible
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), new MockTaskQueuer(tdc), tdc), BID);
        }

        protected T GetGenericController<T, M, S, I>(short BID = TestConstants.BID)
            where T : GenericController<M, S, I>
            where M : class
            where S : BaseGenericService<M>
            where I : struct, IConvertible
        {
            EndorOptions endorOptions = new EndorOptions { };
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), endorOptions), BID);
        }

        protected T GetGenericControllerNoOptions<T, M, S, I>(short BID = TestConstants.BID)
            where T : GenericController<M, S, I>
            where M : class
            where S : BaseGenericService<M>
            where I : struct, IConvertible
        {
            EndorOptions endorOptions = new EndorOptions { };
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient()), BID);
        }


        protected ApiContext GetApiContext(short BID = TestConstants.BID)
        {
            var result = new ApiContext(new DbContextOptionsBuilder<ApiContext>().Options, GetMockTenantDataCache(), BID);
            result.Database.Migrate();
            return result;
        }

        protected T AuthorizeTestController<T>(T controller, short BID = TestConstants.BID)
            where T : Controller
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                 new Claim(ClaimNameConstants.UserID, AuthUserID.ToString()),
                 new Claim(ClaimNameConstants.BID, BID.ToString()),
                 new Claim(ClaimNameConstants.UserLinkID, UserLinkID.ToString()),
                 new Claim(ClaimNameConstants.EmployeeID, TestConstants.AuthEmployeeID.ToString())
            }));
            var httpContext = new DefaultHttpContext() { User = user };
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext
            };

            /*
            controller.ObjectValidator = new DefaultObjectValidator(
                new DefaultModelMetadataProvider(
                    new DefaultCompositeMetadataDetailsProvider(
                        new List<IMetadataDetailsProvider>(new MvcOptions().ModelMetadataDetailsProviders) { })),
                new List<IModelValidatorProvider>() { new DefaultModelValidatorProvider() });
            */
            return controller;
        }
        
        protected void AssertHasTestBusiness()
        {
            var ctx = GetApiContext();
            var business = ctx.BusinessData.FirstOrDefault(x => x.BID == TestConstants.BID);
            if (business == null)
            {
                var templateBusiness = ctx.Set<BusinessData>().AsNoTracking().FirstOrDefault();
                if (templateBusiness == null)
                    throw new InvalidOperationException("No business in table to work off of!");
                else
                {
                    templateBusiness.BID = TestConstants.BID;
                    ctx.BusinessData.Add(templateBusiness);
                    ctx.SaveChanges();
                    business = ctx.BusinessData.FirstOrDefault(x => x.BID == TestConstants.BID);
                    Assert.IsNotNull(business);
                }
            }
        }

        private static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            using (Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext))
            {
                if (regKey != null)
                {
                    var contentType = regKey.GetValue("Content Type");

                    mimeType = contentType != null ? contentType.ToString() : mimeType;
                }
            }
            return mimeType;
        }
    }
}
