﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Common.Classes;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Controllers;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Endor.BoardApi.Tests
{
    [TestClass]
    public class BoardDefinitionTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/Board/Definition";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestBoardDefinitionsCRUD()
        {
            #region CREATE

            var testEntity = Utils.GetBoardDefinitionData();
            var createdEntity = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get All
            var retrievedEntities = await EndpointTests.AssertGetStatusCode<BoardDefinitionData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEntities);

            //test DataType
            retrievedEntities = await EndpointTests.AssertGetStatusCode<BoardDefinitionData[]>(client, $"{apiUrl}/?DataType={(int)DataType.Boolean}", HttpStatusCode.OK);
            Assert.IsTrue(retrievedEntities.Any(t => t.ID == createdEntity.ID));

            // Test Get By ID
            var retrievedEntity = await EndpointTests.AssertGetStatusCode<BoardDefinitionData>(client, $"{apiUrl}/{createdEntity.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEntity);

            #endregion

            #region UPDATE

            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{invalidID}", JsonConvert.SerializeObject(createdEntity), HttpStatusCode.NotFound, HttpStatusCode.BadRequest);

            var newDescription = "TEST DESCRIPTION CHANGE " + DateTime.UtcNow;
            createdEntity.Description = newDescription;
            var putEntity = await EndpointTests.AssertPutStatusCode<BoardDefinitionData>(client, $"{apiUrl}/{createdEntity.ID}", JsonConvert.SerializeObject(createdEntity), HttpStatusCode.OK);
            Assert.IsNotNull(putEntity);
            Assert.AreEqual(newDescription, putEntity.Description);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity.ID}", HttpStatusCode.NoContent);

            // Confirm deletion
            retrievedEntity = await EndpointTests.AssertGetStatusCode<BoardDefinitionData>(client, $"{apiUrl}/{createdEntity.ID}", HttpStatusCode.NotFound);
            Assert.IsNull(retrievedEntity);

            #endregion

        }

        /// <summary>
        /// test for <see cref="BoardDefinitionController"/>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardDefinitionActions()
        {
            var ctx = GetApiContext();

            var testEntity = Utils.GetBoardDefinitionData();
            var createdEntity = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity.ID}/action/setactive", HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdEntity.ID}/action/candelete", HttpStatusCode.OK);

            var module = Module.Accounting;
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);
            //POST Favorite action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity.ID}/action/favorite/module/{module}/{emp.ID}", HttpStatusCode.OK);

            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{createdEntity.ID}/action/isfavorite/module/{module}/{emp.ID}", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            //POST Favorite action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity.ID}/action/unfavorite/module/{module}/{emp.ID}", HttpStatusCode.OK);
            statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{createdEntity.ID}/action/isfavorite/module/{module}/{emp.ID}", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsFalse(statusResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity.ID}", HttpStatusCode.NoContent);
        }

        /// <summary>
        /// test for <see cref="BoardDefinitionController"/>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardEmployeeDefinitionActions()
        {
            var ctx = GetApiContext();

            var testEntity1 = Utils.GetBoardDefinitionData();
            var testEntity2 = Utils.GetBoardDefinitionData();
            var testEntity3 = Utils.GetBoardDefinitionData();
            var createdEntity1 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity1), HttpStatusCode.OK);
            var createdEntity2 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity2), HttpStatusCode.OK);
            var createdEntity3 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity3), HttpStatusCode.OK);
            var entries = ctx.EmployeeData.Where(x => x.ID < -95);
            ctx.RemoveRange(entries);
            ctx.SaveChanges();
            //create Employee
            var employee1 = new EmployeeData()
            {
                ID = -99,
                BID = 1,
                IsActive = true,
                First = "John",
                Last = "Doe",
                LocationID = 1
            };
            ctx.EmployeeData.Add(employee1);
            await ctx.SaveChangesAsync();
            //POST action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/linkemployee/{employee1.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity2.ID}/action/linkemployee/{employee1.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity3.ID}/action/linkemployee/{employee1.ID}?sortIndex=2", HttpStatusCode.OK);

            var boardLinkList = ctx.BoardEmployeeLink.Where(x => x.EmployeeID == employee1.ID).ToList().OrderBy(x => x.SortIndex).ToArray();
            Assert.AreEqual(boardLinkList[0].BoardID, createdEntity1.ID);
            Assert.AreEqual(boardLinkList[1].BoardID, createdEntity3.ID);
            Assert.AreEqual(boardLinkList[2].BoardID, createdEntity2.ID);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/unlinkemployee/{employee1.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity2.ID}/action/unlinkemployee/{employee1.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity3.ID}/action/unlinkemployee/{employee1.ID}", HttpStatusCode.OK);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity2.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity3.ID}", HttpStatusCode.NoContent);
            ctx = GetApiContext();
            entries = ctx.EmployeeData.Where(x => x.ID < -95);
            ctx.EmployeeData.RemoveRange(entries);
            await ctx.SaveChangesAsync();
            
        }

        /// <summary>
        /// test for <see cref="BoardDefinitionController"/>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardRoleActions()
        {
            var ctx = GetApiContext();

            var testEntity1 = Utils.GetBoardDefinitionData();
            var createdEntity1 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity1), HttpStatusCode.OK);
            var entries = ctx.EmployeeRole.Where(x => x.ID == -99);
            ctx.RemoveRange(entries);
            ctx.SaveChanges();
            //create Employee
            var role1 = new EmployeeRole()
            {
                ID = -99,
                AllowMultiple = true,
                AllowOnTeam = true,
                BID = 1,
                ClassTypeID = ClassType.EmployeeRole.ID(),
                EstimateDestinationRestriction = RoleAccess.NotAllowed,
                EstimateItemRestriction = RoleAccess.NotAllowed,
                EstimateRestriction = RoleAccess.NotAllowed,
                IsActive = true,
                IsSystem = false,
                ModifiedDT = DateTime.UtcNow,
                Name = "Test",
                OpportunityRestriction = RoleAccess.NotAllowed,
                OrderDestinationRestriction = RoleAccess.NotAllowed,
                OrderItemRestriction = RoleAccess.NotAllowed,
                OrderRestriction = RoleAccess.NotAllowed,
                PORestriction = RoleAccess.NotAllowed,
            };
            ctx.EmployeeRole.Add(role1);
            await ctx.SaveChangesAsync();
            var boardLinkList = ctx.BoardRoleLink.Where(x => x.RoleID == role1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 0);
            //POST action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/linkrole/{role1.ID}", HttpStatusCode.OK);
            
            boardLinkList = ctx.BoardRoleLink.Where(x => x.RoleID == role1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 1);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/unlinkrole/{role1.ID}", HttpStatusCode.OK);

            boardLinkList = ctx.BoardRoleLink.Where(x => x.RoleID == role1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 0);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.NoContent);
            ctx = GetApiContext();
            entries = ctx.EmployeeRole.Where(x => x.ID < -95);
            ctx.EmployeeRole.RemoveRange(entries);
            await ctx.SaveChangesAsync();

        }

        /// <summary>
        /// test for <see cref="BoardDefinitionController"/>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardModuleActions()
        {
            var ctx = GetApiContext();

            var testEntity1 = Utils.GetBoardDefinitionData();
            var createdEntity1 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity1), HttpStatusCode.OK);

            var module = Module.Accounting;
            var boardLinkList = ctx.BoardModuleLink.Where(x => x.ModuleType == module && x.BoardID == createdEntity1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 0);
            //POST action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/linkmodule/{(short)module}", HttpStatusCode.OK);

            boardLinkList = ctx.BoardModuleLink.Where(x => x.ModuleType == module && x.BoardID == createdEntity1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 1);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEntity1.ID}/action/unlinkmodule/{(short)module}", HttpStatusCode.OK);

            boardLinkList = ctx.BoardModuleLink.Where(x => x.ModuleType == module && x.BoardID == createdEntity1.ID).ToList();
            Assert.AreEqual(boardLinkList.Count(), 0);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.NoContent);
        }


        /// <summary>
        /// test for <see cref="BoardDefinitionController"/>
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardModuleLinkCreateUpdate()
        {
            var ctx = GetApiContext();

            var testEntity1 = Utils.GetBoardDefinitionData();
            Module module = Module.Management;
            testEntity1.ModuleLinks = new List<BoardModuleLink>()
            {
                new BoardModuleLink(){
                    ModuleType = module
                }
            };
            var createdEntity1 = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEntity1), HttpStatusCode.OK);

            Assert.IsNotNull(createdEntity1.ModuleLinks);
            Assert.AreEqual(1, createdEntity1.ModuleLinks.Count);

            var createdLink = ctx.BoardModuleLink.FirstOrDefault(x => x.BoardID == createdEntity1.ID && x.ModuleType == module);
            Assert.IsNotNull(createdLink);

            var newModule = Module.Accounting;
            createdEntity1.ModuleLinks.Add(new BoardModuleLink(){
                ModuleType = newModule
            });

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", createdEntity1, HttpStatusCode.OK);

            var retrievedEntity = await EndpointTests.AssertGetStatusCode<BoardDefinitionData>(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEntity);
            Assert.IsNotNull(retrievedEntity.ModuleLinks);
            Assert.AreEqual(2, retrievedEntity.ModuleLinks.Count);
            BoardModuleLink accountingLink = retrievedEntity.ModuleLinks.FirstOrDefault(y => y.ModuleType == newModule);
            Assert.IsNotNull(accountingLink);

            retrievedEntity.ModuleLinks.Remove(accountingLink);

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", retrievedEntity, HttpStatusCode.OK);

            var updatedEntity = await EndpointTests.AssertGetStatusCode<BoardDefinitionData>(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(updatedEntity);
            Assert.IsNotNull(updatedEntity.ModuleLinks);
            Assert.AreEqual(1, updatedEntity.ModuleLinks.Count);
            accountingLink = retrievedEntity.ModuleLinks.FirstOrDefault(y => y.ModuleType == newModule);
            Assert.IsNull(accountingLink);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEntity1.ID}", HttpStatusCode.NoContent);
        }

        

        /// <summary>
        /// test
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestBoardDataSummary()
        {

            var retrievedEntities = await EndpointTests.AssertGetStatusCode<BoardSummaryStatus[]>(client, $"/api/board/summary", HttpStatusCode.OK);
        }
    }
}
