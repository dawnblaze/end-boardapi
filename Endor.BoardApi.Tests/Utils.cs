﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.BoardApi.Tests
{
    public static class Utils
    {
        public static int GetHashIDForTestMethod([System.Runtime.CompilerServices.CallerMemberName]string memberName = "")
        {
            //ABS fouls up the hash code but entityClient regexes assume positive IDs
            return Math.Abs(memberName.GetHashCode());
        }

        internal static DomainEmail GetDomainEmail()
        {
            return new DomainEmail()
            {
                BID = 1,
                IsActive = true,
                DomainName = "domain.tld",
                ProviderType = EmailProviderType.CustomSMTP
            };
        }

        /// <summary>
        /// You will need to set  
        /// CompanyID, PickupLocationID 
        /// TaxGroupID, ProductionLocationID
        /// </summary>
        /// <returns></returns>
        public static OrderData GetTestOrderData()
        {
            return new OrderData()
            {
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                LocationID = 1,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
            };
        }

        public static FlatListItem GetFlatListItem()
        {
            return new FlatListItem()
            {
                ClassTypeID = (int)ClassType.FlatListItem,
                FlatListType = FlatListType.TaxExemptReasons,
                IsActive = true,
                IsAdHoc = false,
                IsSystem = true,
                ModifiedDT = DateTime.UtcNow,
                Name = "FlatListItemSerializationTest" + DateTime.Now.ToString("yyyyMMddHHmmssffff")
            };
        }

        internal static OrderDestinationData GetTestOrderDestinationData(string testID, short destNumber, short orderItemStatusID, int orderItemID, int orderID, decimal priceTax, OrderTransactionType transactionType)
        {
            return new OrderDestinationData()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.OrderDestination,
                ComputedNet = null,
                Description = $"{nameof(OrderDestinationData.Description)}{testID}",
                DestinationNumber = destNumber,
                DestinationType = OrderDestinationType.OnSiteService,
                HasDocuments = false,
                IsForAllItems = true,
                IsTaxExempt = false,
                ItemStatusID = orderItemStatusID,
                Name = $"{nameof(OrderDestinationData.Name)}{testID}",
                OrderID = orderID,
                OrderStatusID = OrderOrderStatus.DestinationReady,
                PriceIsLocked = false,
                PriceNetOV = false,
                PriceTax = priceTax,
                PriceTaxableOV = false,
                TransactionType = (byte)transactionType
            };
        }

        public static OrderItemData GetTestOrderItemData(OrderData createdOrder)
        {
            return new OrderItemData()
            {
                BID = 1,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = createdOrder.ID,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = createdOrder.OrderStatusID,
                ItemStatusID = 18,
                ProductionLocationID = createdOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = createdOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = createdOrder.TransactionType
            };
        }

        public static OrderOrderLink GetOrderOrderLink(TransactionHeaderData createdOrder, TransactionHeaderData clonedOrder)
        {
            var result = new OrderOrderLink()
            {
                BID = createdOrder.BID,
                Description = $"{OrderOrderLinkType.ClonedTo.ToString()} {clonedOrder.FormattedNumber}",
                OrderID = createdOrder.ID,
                LinkedOrderID = clonedOrder.ID,
                LinkedFormattedNumber = clonedOrder.FormattedNumber,
            };

            return result;
        }

        internal static async Task<List<SimpleEmployeeData>> GetEmployees(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "api/employee/simplelist", HttpStatusCode.OK)).ToList();
        }

        public static OrderContactRole GetOrderContactRole()
        {
            var result = new OrderContactRole()
            {
                ContactName = "Test Contact 1",
                RoleType = OrderContactRoleType.Primary,
            };

            return result;
        }

        public static OrderEmployeeRole GetOrderEmployeeRole()
        {
            var result = new OrderEmployeeRole()
            {
                EmployeeID = 101,
                RoleID = 1,
            };

            return result;
        }

        public static async Task<int> GetCompanyID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<int> GetContactID(HttpClient client, int? companyId = null)
        {
            if (companyId.HasValue)
                return (await EndpointTests.AssertGetStatusCode<SimpleContactData[]>(client, $"api/contact/simplelist?CompanyID={companyId.Value}", HttpStatusCode.OK)).FirstOrDefault().ID;

            return (await EndpointTests.AssertGetStatusCode<SimpleContactData[]>(client, "api/contact/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<int> GetEmployeeID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "api/employee/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<byte> GetLocationID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<short> GetTaxGroupID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        #region CustomFieldDefinitions
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static CustomFieldDefinition GetCustomFieldDefinition()
        {
            return new CustomFieldDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Contact,
                AppliesToID = null,
                Name = "Test Custom Field Definition",
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false,         
            };
        }
        #endregion

        #region CustomFieldLayoutDefinitions
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinition()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
            };
        }

        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinitionTree()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                Elements = new CustomFieldLayoutElement[] {
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        Row = 1,
                        SortIndex = 1,
                        ElementType = AssemblyElementType.Section,
                        Elements = new CustomFieldLayoutElement[]
                        {
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 1,
                                DataType = (short) DataType.Boolean
                            },
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 2-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 2,
                                DataType = (short) DataType.Boolean
                            }
                        }
                    },
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        Row = 2,
                        ElementType = AssemblyElementType.Section,
                        SortIndex = 2,
                    }
                },
            };
        }

        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinitionTreeWithOneContainerAndOneElement()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                Elements = new CustomFieldLayoutElement[] {
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        Row = 1,
                        SortIndex = 1,
                        ElementType = AssemblyElementType.Section,
                        Elements = new CustomFieldLayoutElement[]
                        {
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 1,
                                DataType = (short) DataType.Boolean,
                                ElementType = AssemblyElementType.Checkbox
                            },
                        }
                    },
                },
            };
        }

        #endregion

        public static BoardDefinitionData GetBoardDefinitionData()
        {
            return new BoardDefinitionData()
            {
                BID = 1,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = (int)ClassType.BoardDefinition,
                DataType = DataType.Boolean,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition Description",
                IsAlwaysShown = true,
                LimitToRoles = false
            };
        }

        public static DashboardData GetTestDashboard(string name = "Dashboard.UnitTest")
        {
            return new DashboardData()
            {
                BID = 1,
                ClassTypeID = ClassType.Dashboard.ID(),
                IsActive = true,
                ModifiedDT = DateTime.UtcNow,
                Module = Module.Sales,
                Name = name,
                UserLinkID = 1
            };
        }

        public static DashboardWidgetData GetTestDashboardWidget(string name = "Dashboard.Widget.UnitTest", short widgetDefinitionID = 1, short dashboardID = 1)
        {
            return new DashboardWidgetData()
            {
                BID = 1,
                IsActive = true,
                Name = name,
                WidgetDefinitionID = widgetDefinitionID,
                DashboardID = dashboardID
            };
        }
    }
}
