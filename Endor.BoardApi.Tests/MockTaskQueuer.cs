﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.Tasks;
using Endor.Tenant;

namespace Endor.BoardApi.Tests
{
    internal class MockTaskQueuer : ITaskQueuer
    {
        public string Priority { get; set; }
        public string Schedule { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public MockTaskQueuer(ITenantDataCache cache)
        {
        }

        public Task<string> EmptyTemp(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> EmptyTrash(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexClasstype(short bid, int ctid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexModel(short bid, int ctid, int id)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> Test(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> CheckOrderStatus(short bid, int orderId, int status)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> OrderPricingCompute(short bid, int orderID)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> OrderPricingComputeTax(short bid, int orderID)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> DeleteExpiredDrafts(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> RepairUntrustedConstraint(short bid)
        {
            return Task.FromResult("sure");
        }

        public Task<string> RecomputeGL(short bid, byte EnumGLType, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> RegenerateCBELForMachine(short bid, int machineId)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> ClearAllAssemblies(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> ClearAssembly(short bid, int ctid, int id)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> CreateDBBackup(short bid, DbType dbType)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments)
        {
            throw new NotImplementedException();
        }

        public Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
        {
            throw new NotImplementedException();
        }
    }
}