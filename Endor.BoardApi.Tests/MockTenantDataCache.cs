﻿using System.Threading.Tasks;
using Endor.Tenant;

namespace Endor.BoardApi.Tests
{
    public class MockTenantDataCache : ITenantDataCache
    {
        private readonly string _sql;
        private readonly string _blob;

        public MockTenantDataCache(string sql, string blobStorage)
        {
            this._sql = sql;
            this._blob = blobStorage;
        }

        public Task<TenantData> Get(short bid)
        {
            return Task.FromResult(new TenantData()
            {
                BusinessDBConnectionString = _sql,
                StorageConnectionString = _blob,
                LoggingURL = "http://fakeurl.com",
                LoggingDBConnectionString = "FakeConnectionString",
            });
        }

        public void InvalidateCache()
        {
            
        }

        public void InvalidateCache(short bid)
        {
            
        }
    }
}
