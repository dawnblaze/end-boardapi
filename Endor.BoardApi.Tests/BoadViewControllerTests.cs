﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Common.Classes;
using Endor.BoardApi.Web.Controllers;
using Endor.BoardApi.Web.Services;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using AssertX = Xunit.Assert;

namespace Endor.BoardApi.Tests
{
    //class BoadViewControllerTests
    //{
    //}

    [TestClass]
    public class BoadViewControllerTests : CommonControllerTestClass
    {
        #region Test Setup/Initialize/Cleanup

        public const string apiUrl = "Api/Board/View";
        public const string bdApiUrl = "/Api/Board/Definition";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string newBoarView;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            newBoarView = DateTime.UtcNow + " TEST Board View";
        }

        [TestCleanup]
        public void Teardown()
        {
            DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleBoardView[] result = JsonConvert.DeserializeObject<SimpleBoardView[]>(responseString);
                if (result != null)
                {
                    SimpleBoardView boardView = result.FirstOrDefault(x => x.DisplayName.Contains(newBoarView));
                    if (boardView != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{boardView.ID}");
                    }
                }
            }).Wait();
        }

        private BoardView GetTestBoardView()
        {
            return new BoardView()
            {
                IsActive = true,
                Name = this.newBoarView,
                ClassTypeID = (int)ClassType.BoardView,
                ModifiedDT = DateTime.UtcNow,
            };
        }

        private BoardDefinitionData GetBoardDefinitionData()
        {
            return new BoardDefinitionData()
            {
                BID = 1,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = (int)ClassType.BoardDefinition,
                DataType = DataType.Boolean,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition Description",
                IsAlwaysShown = true,
                LimitToRoles = false
            };
        }


        #endregion

        [TestMethod]
        public async Task TestBoardViewCRUDEndpoints()
        {
            var newModel = GetTestBoardView();
            var newBoardView = await EndpointTests.AssertPostStatusCode<BoardView>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=false", HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=false", HttpStatusCode.OK);

            var getBoardView = await EndpointTests.AssertGetStatusCode<BoardView>(client, $"{apiUrl}/{newBoardView.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getBoardView);

            getBoardView.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{getBoardView.ID}", JsonConvert.SerializeObject(getBoardView), HttpStatusCode.OK);

            getBoardView = await EndpointTests.AssertGetStatusCode<BoardView>(client, $"{apiUrl}/{newBoardView.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getBoardView);
            Assert.IsFalse(getBoardView.IsActive);

            var cloneBoardView = await EndpointTests.AssertPostStatusCode<BoardView>(client, $"{apiUrl}/{getBoardView.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloneBoardView);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneBoardView.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{getBoardView.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestBoardViewSetActiveSetInactive()
        {
            var boardView = GetTestBoardView();

            //test: expect OK
            boardView = await EndpointTests.AssertPostStatusCode<BoardView>(client, $"{apiUrl}", JsonConvert.SerializeObject(boardView), HttpStatusCode.OK);
            Assert.IsNotNull(boardView);

            // set inactive
            var statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{boardView.ID}/Action/SetInactive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            var boardViewTest = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{boardView.ID}", HttpStatusCode.OK);
            Assert.IsFalse(boardViewTest.IsActive);

            // set active
            statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{boardView.ID}/Action/SetActive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            // confirm boardView is now active again
            boardViewTest = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{boardView.ID}", HttpStatusCode.OK);
            Assert.IsTrue(boardViewTest.IsActive);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{boardView.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestBoardViewCanDelete()
        {
            var boardView = GetTestBoardView();

            //test: expect OK
            boardView = await EndpointTests.AssertPostStatusCode<BoardView>(client, $"{apiUrl}", JsonConvert.SerializeObject(boardView), HttpStatusCode.OK);
            Assert.IsNotNull(boardView);


            // check CanDelete
            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{boardView.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            var boardViewID = 0;
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleBoardView[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleBoardView sbv in simpleList)
            {
                if (sbv.DisplayName == boardView.Name)
                {
                    boardViewID = sbv.ID;
                }
            }

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + boardViewID, JsonConvert.SerializeObject(boardView), HttpStatusCode.OK);

            statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{boardViewID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{boardViewID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestBoardViewDeleteLinks()
        {
            // GET api/Board/View/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleBoardView[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            // confirm that a board view of name "Create Board View Test" doesn't already exist in the db
            var sld = simpleList.FirstOrDefault(s => s.DisplayName == "Create Board View Test");
            if (sld != null)
            {
                // remove "Create Board View Test" record
                await EndpointTests.AssertDeleteStatusCode<SimpleBoardView[]>(client, $"{apiUrl}/{sld.ID}", HttpStatusCode.NoContent);
                // confirm location was deleted
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{sld.ID}", HttpStatusCode.NotFound);
            }

            var board = new BoardView()
            {
                BID = 1,
                Name = "Create Board View",
                IsActive = false,
            };

            // POST api/board/view
            var newBoard = await EndpointTests.AssertPostStatusCode<BoardView>(client, apiUrl, JsonConvert.SerializeObject(board), HttpStatusCode.OK);

            var boardView = GetTestBoardView();

            //create BoardDefinitionData
            var testEntity = Utils.GetBoardDefinitionData();
            var createdEntity = await EndpointTests.AssertPostStatusCode<BoardDefinitionData>(client, $"{bdApiUrl}/", JsonConvert.SerializeObject(testEntity), HttpStatusCode.OK);

            var retrievedEntities = await EndpointTests.AssertGetStatusCode<BoardDefinitionData[]>(client, $"{bdApiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEntities);

            boardView.BoardViewLinks = new List<BoardViewLink>();
            var bvl = new BoardViewLink
            {
                BID = 1,
                BoardID = retrievedEntities.FirstOrDefault().ID,
                ViewID = boardView.ID,
                SortIndex = 1,
            };
            //test: expect OK
            boardView.BoardViewLinks.Add(bvl);
            boardView = await EndpointTests.AssertPostStatusCode<BoardView>(client, $"{apiUrl}", JsonConvert.SerializeObject(boardView), HttpStatusCode.OK);
            Assert.IsNotNull(boardView);

            var retrievedBoardView = await EndpointTests.AssertGetStatusCode<BoardView>(client, $"{apiUrl}/{boardView.ID}", HttpStatusCode.OK);
            Assert.IsTrue(retrievedBoardView.BoardViewLinks.Count > 0);
            Assert.IsTrue(retrievedBoardView.Boards.Count > 0);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{boardView.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newBoard.ID}", HttpStatusCode.NoContent);
        }

    }
}
