﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Acheve.AspNetCore.TestHost.Security;
using Endor.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Endor.BoardApi.Tests
{
    /// <summary>
    /// Used for testing API Endpoints
    /// </summary>
    public static class EndpointTests
    {
        /// <summary>
        /// Gets an HttpClient for use with the other EndpointTests methods
        /// </summary>
        /// <returns></returns>
        public static HttpClient GetHttpClient()
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.BoardApi.Web\\";
            var builder = new WebHostBuilder()
                .CaptureStartupErrors(true)
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();

            var testServer = new TestServer(builder);

            return testServer.CreateClient().WithDefaultIdentity(new[] {
                new Claim(ClaimNameConstants.BID, "1"),
                new Claim(ClaimNameConstants.UserID, "1"),
                new Claim(ClaimNameConstants.EmployeeID, "1"),
                new Claim(ClaimNameConstants.UserLinkID, "1"),
            });
        }

        /// <summary>
        /// Gets an HttpClient for use with the other EndpointTests methods
        /// </summary>
        /// <param name="userID">UserID to set the Claims for</param>
        /// <returns></returns>
        public static HttpClient GetHttpClient(int userID)
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.BoardApi.Web\\";
            var builder = new WebHostBuilder()
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();

            var testServer = new TestServer(builder);

            return testServer.CreateClient().WithDefaultIdentity(new[] {
                new Claim(ClaimNameConstants.BID, "1"),
                new Claim(ClaimNameConstants.UserID, userID.ToString()),
                new Claim(ClaimNameConstants.EmployeeID, "1"),
                new Claim(ClaimNameConstants.UserLinkID, "1"),
            });
        }

        /// <summary>
        /// Tests against GET response's content
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="path">Path</param>
        /// <param name="content">Content to test against</param>
        /// <returns></returns>
        public static async Task AssertGetResponseContent(HttpClient client, string path, string content)
        {
            HttpResponseMessage response = await client.GetAsync(path);
            // Fail the test if non-success result
            response.EnsureSuccessStatusCode();

            // Get the response as a string
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(responseString.Contains(content), $"GET { path} content did not contain string {content}");
        }

        public static async Task AssertGetResponseContent(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            Assert.AreEqual<HttpStatusCode>(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");

            // Get the response as a string
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(responseString.Contains(content), $"GET { url} content did not contain string {content}");
        }

        /// <summary>
        /// Tests against GET response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertGetStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");
        }

        /// <summary>
        /// Tests against GET response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertGetStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match expected status code");

            // skip checking when bad request is expected
            if (statusCode.Equals(HttpStatusCode.NotFound))
            {
                return default(T);
            }

            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"GET {url} had null content");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"GET {url} content did deserialize into a {typeof(T).Name}");

            return result;
        }



        /// <summary>
        /// Tests against either of GET response's Status Codes.
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCodeOne">HTTPStatusCode 1 to test for</param>
        /// <param name="statusCodeTwo">HTTPStatusCode 2 to test for</param>
        /// <returns></returns>
        public static async Task AssertGetEitherStatusCode(HttpClient client, string url, HttpStatusCode statusCodeOne, HttpStatusCode statusCodeTwo)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.IsTrue(response.StatusCode == statusCodeOne || response.StatusCode == statusCodeTwo, $"GET {url} did not match status code {statusCodeOne} or {statusCodeTwo}");
        }

        /// <summary>
        /// Tests against POST response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content Object to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertPostStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            await AssertPostStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        /// <summary>
        /// Tests against POST response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(url, strContent);
            Assert.AreEqual(statusCode, response.StatusCode, $"POST {url} did not match status code {statusCode}");
            return response;
        }

        public static async Task<HttpResponseMessage> AssertPostStatusCodeEither(HttpClient client, string url, string content, HttpStatusCode statusCode, HttpStatusCode altStatusCode)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(url, strContent);
            Assert.IsTrue(statusCode == response.StatusCode || altStatusCode == response.StatusCode, $"POST {url} did not match status code {statusCode} or {altStatusCode}");
            return response;
        }
        public static async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PostAsync(url, null);
            Assert.AreEqual(statusCode, response.StatusCode, $"POST {url} did not match status code {statusCode}");
            return response;
        }

        /// <summary>
        /// Tests against POST response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPostStatusCode(client, url, content, statusCode);

            // skip deserializing response if we expect a bad request
            if (statusCode == HttpStatusCode.BadRequest)
            {
                return default(T);
            }

            string responseString = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"POST {url} content was null");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"POST {url} content did not deserialize into {typeof(T).Name}");

            return result;
        }

        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPostStatusCode<T>(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPostStatusCode(client, url, statusCode);

            string responseString = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"POST {url} content was null");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"POST {url} content did not deserialize into {typeof(T).Name}");

            return result;
        }

        /// <summary>
        /// Tests against PUT response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPutStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode, HttpStatusCode? statusCodeAlternative = null)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(url, strContent);
            Assert.IsTrue(response.StatusCode == statusCode || (statusCodeAlternative.HasValue && response.StatusCode == statusCodeAlternative.Value), $"PUT {url} did not match status code {statusCode} or {statusCodeAlternative}");

            return response;
        }

        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PutAsync(url, null);
            T result = await HandleResponseMessage<T>(response);
            return result;
        }

        /// <summary>
        /// Tests against PUT response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPutStatusCode(client, url, content, statusCode);
            T result = await HandleResponseMessage<T>(response);

            return result;
        }

        /// <summary>
        /// Deserializes response from endpoint into type of T. Optionally allows for NoContentResponse e.g. from DELETE endpoints
        /// </summary>
        /// <typeparam name="T">Type of object to be used for deserialization</typeparam>
        /// <param name="response">HttpResponseMessage received from API</param>
        /// <param name="allowNoContentResponse">Set this to 'true' when expecting NoContentResponse or empty response body. Defaults to false</param>
        /// <returns></returns>
        private static async Task<T> HandleResponseMessage<T>(HttpResponseMessage response, bool allowNoContentResponse = false)
        {
            T result;
            string verb = response.RequestMessage.Method.Method;
            string responseString = await response.Content.ReadAsStringAsync();

            //Special handling of Delete endpoints that return NoContentResult
            if (!allowNoContentResponse)
            {
                Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content was null");
                result = JsonConvert.DeserializeObject<T>(responseString);
                Assert.IsNotNull(result, $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content did not deserialize into {typeof(T).Name}");
            }
            else
            {//Create instance of NoContentResult or expected return type of T
                result = Activator.CreateInstance<T>();
            }

            return result;
        }

        /// Tests against PUT response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content Object to PUT</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertPutStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            await AssertPutStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }
        public static async Task AssertPutStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            await AssertPutStatusCode(client, url, null, statusCode);
        }

        /// <summary>
        /// Tests against DELETE response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertDeleteStatusCode(HttpClient client, string url, params HttpStatusCode[] statusCodes)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(url);
                //Assert.AreEqual(statusCode, response.StatusCode);
                AssertAtLeastOneMatch(response, statusCodes);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void AssertAtLeastOneMatch(HttpResponseMessage response, HttpStatusCode[] statusCodes)
        {
            Assert.IsNotNull(response);
            if (statusCodes == null)
                throw new ArgumentNullException("statusCodes");
            else if (statusCodes.Length == 0)
                throw new InvalidOperationException("Must pass at least one expected status code");
            else if (statusCodes.Length == 1)
                Assert.AreEqual(statusCodes[0], response.StatusCode);
            else
            {
                if (!statusCodes.Any(x => response.StatusCode == x))
                {
                    string expected = String.Join(" or ", statusCodes);
                    Assert.Fail($"Expected {expected}, instead got {response.StatusCode}");
                }
            }
        }

        public static async Task<T> AssertDeleteStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            var response = await AssertDeleteStatusCode(client, url, statusCode);
            return await HandleResponseMessage<T>(response, allowNoContentResponse: true);
        }

        public static async Task<T> AssertDeleteStatusCodeWithContent<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            var response = await AssertDeleteStatusCode(client, url, statusCode);
            return await HandleResponseMessage<T>(response);
        }
    }
}
