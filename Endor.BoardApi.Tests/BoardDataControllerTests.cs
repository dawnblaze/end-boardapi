﻿using Endor.BoardApi.Web.Classes;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.BoardApi.Tests
{
    [TestClass]
    public class BoardDataControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/Board";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task BoardDataSecurityTests()
        {
            #region Initial Setup of Entities

            var currDateStr = DateTime.UtcNow.ToString();
            var ctx = GetApiContext();

            var employee = new EmployeeData()
            {
                BID = 1,
                ID = -99,
                First = "UnitTest",
                Last = currDateStr,
                IsActive = true,
                LocationID = ctx.LocationData.FirstOrDefault(l => l.BID == 1).ID
            };

            var userLink = new UserLink()
            {
                BID = 1,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = employee.ID,
                UserAccessType = UserAccessType.Employee
            };

            var board = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                Name = $"UnitTest.{currDateStr}",
                DataType = DataType.LineItem,
                ConditionFx = "{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 8,\"LhsDataType\": 3,\"RhsDataType\": 3,\"Terms\": [{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 6,\"LhsDataType\": 3,\"RhsDataType\": 3,\"Terms\": [{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 0,\"LhsDataType\": 1,\"RhsDataType\": -1,\"Terms\": [{\"$type\": \"AELMember\",\"DataType\": 3,\"Text\": \"Tax Exempt\",\"ID\": 20391,\"MemberBase\": {\"$type\": \"AELMember\",\"DataType\": 20390,\"Text\": \"LineItem\",\"ID\": 10032,\"MemberBase\": {\"$type\": \"AELMember\",\"DataType\": 20040,\"Text\": \"LineItem\",\"ID\": -1,\"MemberBase\": null}},\"RelatedID\": 0},{\"$type\": \"AELValues\",\"DataType\": 3,\"Values\": [false],\"DisplayText\": false}]}]}]}"
            };

            try
            {
                await TextRecordsCleanup(ctx, employee, userLink, board);
            }
            catch (Exception)
            {
                throw;
            }
            #endregion

            try
            {
                #region Create DB Records

                ctx.EmployeeData.Add(employee);
                ctx.UserLink.Add(userLink);
                ctx.BoardDefinitionData.Add(board);
                Assert.AreEqual(3, await ctx.SaveChangesAsync());

                #endregion

                client = EndpointTests.GetHttpClient(userLink.AuthUserID.Value);

                var expectPass = await EndpointTests.AssertGetStatusCode<BoardData<OrderItemBoardItem>>(client,
                    $"{apiUrl}/{board.ID}/Data", HttpStatusCode.OK);

                Assert.IsNotNull(expectPass);

                userLink.UserAccessType = UserAccessType.Contact;
                ctx.UserLink.Update(userLink);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{board.ID}/Data", HttpStatusCode.Unauthorized);

                userLink.UserAccessType = UserAccessType.ContactAdministrator; ctx.UserLink.Update(userLink);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{board.ID}/Data", HttpStatusCode.Unauthorized);

                userLink.UserAccessType = UserAccessType.None;
                ctx.UserLink.Update(userLink);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{board.ID}/Data", HttpStatusCode.Unauthorized);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            finally
            {
                #region Clean-Up Test Entities
                await TextRecordsCleanup(ctx, employee, userLink, board);
                #endregion Clean-Up Test Entities
            }

        }

        private static async Task TextRecordsCleanup(EF.ApiContext ctx, EmployeeData employee, UserLink userLink, BoardDefinitionData board)
        {
            board = ctx.BoardDefinitionData.FirstOrDefault(x => x.ID == board.ID);
            if (board != null)
                ctx.BoardDefinitionData.Remove(board);

            userLink = ctx.UserLink.FirstOrDefault(x => x.ID == userLink.ID);
            if (userLink != null)
                ctx.UserLink.Remove(userLink);

            employee = ctx.EmployeeData.FirstOrDefault(x => x.ID == employee.ID);
            if (employee != null)
                ctx.EmployeeData.Remove(employee);

            bool saveNeeded = board != null || userLink != null || employee != null;
            
            if(saveNeeded)
                await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task TestBoardDataSummary()
        {
            var retrievedEntities = await EndpointTests.AssertGetStatusCode<BoardSummaryStatus[]>(client, $"{apiUrl}/Summary", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestCompanyAndContactLinks()
        {
            #region Initial Setup of Entities

            var currDateStr = DateTime.UtcNow.ToString();
            var ctx = GetApiContext();

            var employee = new EmployeeData()
            {
                BID = 1,
                ID = -99,
                First = "UnitTest",
                Last = currDateStr,
                IsActive = true,
                LocationID = ctx.LocationData.FirstOrDefault(l => l.BID == 1).ID
            };

            var userLink = new UserLink()
            {
                BID = 1,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = employee.ID,
                UserAccessType = UserAccessType.Employee
            };

            short testBID = TestConstants.BID;
            var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
            if (location == null)
                Assert.Inconclusive("No location data found. Unable to complete test.");

            var companyA = new CompanyData()
            {
                ID = -90,
                Name = "Company A",
                BID = TestConstants.BID,
                IsActive = true,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = 1
            };

            var contactA = new ContactData()
            {
                ID = -91,
                First = "Contact A First",
                Last = "Contact A Last",
                BID = TestConstants.BID
            };
            var contactB = new ContactData()
            {
                ID = -92,
                First = "Contact B First",
                Last = "LastTest2A Contact B Last",
                BID = TestConstants.BID
            };

            var board = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                Name = $"UnitTest.{currDateStr}",
                DataType = DataType.Company,
                ConditionFx = "{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 8,\"LhsDataType\": 3,\"RhsDataType\": 3,\"Terms\": [{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 6,\"LhsDataType\": 3,\"RhsDataType\": 3,\"Terms\": [{\"$type\": \"AELOperation\",\"DataType\": 3,\"Operator\": 0,\"LhsDataType\": 1,\"RhsDataType\": -1,\"Terms\": [{\"$type\": \"AELMember\",\"DataType\": 3,\"Text\": \"Tax Exempt\",\"ID\": 20391,\"MemberBase\": {\"$type\": \"AELMember\",\"DataType\": 20390,\"Text\": \"LineItem\",\"ID\": 10032,\"MemberBase\": {\"$type\": \"AELMember\",\"DataType\": 20040,\"Text\": \"LineItem\",\"ID\": -1,\"MemberBase\": null}},\"RelatedID\": 0},{\"$type\": \"AELValues\",\"DataType\": 3,\"Values\": [false],\"DisplayText\": false}]}]}]}"
            };

            try
            {
                await TextRecordsCleanup(ctx, employee, userLink, board);
            }
            catch (Exception)
            {
                throw;
            }
            #endregion


            try
            {
                #region Create DB Records

                ctx.EmployeeData.Add(employee);
                ctx.UserLink.Add(userLink);
                ctx.BoardDefinitionData.Add(board);

                ctx.CompanyData.Add(companyA);
                ctx.ContactData.AddRange(new[] { contactA, contactB });


                var contactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyA.ID,
                        Roles = ContactRole.Primary,
                        IsActive = true
                    },
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactB.ID,
                        CompanyID = companyA.ID,
                        Roles = ContactRole.Billing,
                        IsActive = true
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinks);

                Assert.AreEqual(8, await ctx.SaveChangesAsync());

                #endregion

                client = EndpointTests.GetHttpClient(userLink.AuthUserID.Value);

                var expectPass = await EndpointTests.AssertGetStatusCode<BoardData<CompanyBoardItem>>(client,
                    $"{apiUrl}/{board.ID}/Data", HttpStatusCode.OK);

                var companyAResult = expectPass.Data.FirstOrDefault(x => x.ID == companyA.ID);
                Assert.IsNotNull(companyAResult);
                Assert.AreEqual(companyAResult.BillingContactID, contactB.ID);
                Assert.AreEqual(companyAResult.PrimaryContactID, contactA.ID);


            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            finally
            {

                var contacts = ctx.ContactData.Where(x => x.ID <= 0);
                var companies = ctx.CompanyData.Where(x => x.ID <= 0);

                ctx.CompanyData.RemoveRange(companies);
                ctx.ContactData.RemoveRange(contacts);
                ctx.SaveChanges();

                #region Clean-Up Test Entities
                await TextRecordsCleanup(ctx, employee, userLink, board);
                #endregion Clean-Up Test Entities
            }
        }
    }
}
