﻿using System.Threading.Tasks;
using Endor.RTM;
using Endor.RTM.Models;

namespace Endor.BoardApi.Tests
{
    internal class MockRealtimeMessagingPushClient : IRTMPushClient
    {
        public MockRealtimeMessagingPushClient()
        {
        }

        public Task SendRefreshMessage(RefreshMessage refreshMsg, string exclude)
        {
            return Task.CompletedTask;
        }

        public Task SendSystemMessage(SystemMessage systemMsg)
        {
            return Task.CompletedTask;
        }

        public Task SendUserMessage(UserMessage userMsg)
        {
            return Task.CompletedTask;
        }

        public Task SendEmployeeMessage(EmployeeMessage employeeMessage)
        {
            return Task.CompletedTask;
        }

        public Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg)
        {
            return Task.CompletedTask;
        }
    }
}