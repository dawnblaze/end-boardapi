﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Claims;
using Acheve.AspNetCore.TestHost.Security;
using Endor.Api.Common;
using Endor.Common;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Endor.BoardApi.Tests
{
    public class TestStartup : BaseStartup
    {
        protected const string SQLConfigName = "SQL";
        protected const string BlobStorageConfigName = "BlobStorage";

        protected IConfigurationRoot _config;

        public TestStartup(IWebHostEnvironment env) : base(env)
        {
        }

        private static Dictionary<string, string> GetLocalSettings()
        {            
            string file = "..\\..\\..\\client-secrets.json";
            if (!File.Exists(file))
            {
                // If you don't have one, we'll make one here.
                File.WriteAllText(file, @"{
    ""SQL"": ""Data Source=DESKTOP-1OQ1RBA\\WORKSQLSERVER;Initial Catalog=\""Dev.Endor.Business.DB1\"";Trusted_Connection=True;"",
    ""BlobStorage"":  ""UseDevelopmentStorage=true""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
            return result;
        }

        public override IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            var configBuilder = base.GetConfigurationBuilder(env);
            configBuilder.AddInMemoryCollection(GetLocalSettings());

            return configBuilder;
        }

        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = TestServerAuthenticationDefaults.AuthenticationScheme;
            })
            .AddTestServerAuthentication();
            services.AddRouting();
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            })
            .AddApplicationPart(Assembly.Load("Endor.BoardApi.Web"))
            .AddApplicationPart(typeof(TestStartup).GetTypeInfo().Assembly)
            .AddControllersAsServices();
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            base.Configure(app, env, loggerFactory, appLifetime);
        }

        public override void ConfigureExternalEndorServices(IServiceCollection services)
        {
            var tdc = GetMockTenantDataCache();
            services.AddSingleton<ITenantDataCache>(tdc);
            services.AddSingleton<ITaskQueuer>(new MockTaskQueuer(tdc));//>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"]));
            services.AddSingleton<RemoteLogger>((x) => new RemoteLogger(tdc)); //new RemoteLogger(tdc));
            services.AddTransient<IRTMPushClient>((x) => new MockRealtimeMessagingPushClient());
            services.AddSingleton<IMigrationHelper, MigrationHelper>();
        }

        private MockTenantDataCache GetMockTenantDataCache()
        {
            var tdc = new MockTenantDataCache(Configuration[SQLConfigName], Configuration[BlobStorageConfigName]);
            return tdc;
        }

        public override string GetAPIName()
        {
            return "Test Board API";
        }

        public override string GetAPIVersion()
        {
            return "v1";
        }
    }
}
