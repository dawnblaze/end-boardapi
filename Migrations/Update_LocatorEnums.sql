begin transaction

alter table [enum.Locator.Type] drop column RegEx
alter table [enum.Locator.Type] add ValidityRegEx varchar(max) null
alter table [enum.Locator.Type] add PossibilityRegEx varchar(max) null
alter table [enum.Locator.Type] add PossibilitySortOrder tinyint null

EXEC sp_rename '[enum.Locator.SubType].[LocatorSubType]', 'ID', 'COLUMN'
alter table [enum.Locator.SubType] drop column RegEx
alter table [enum.Locator.SubType] add LinkFormatString varchar(255) null

commit

