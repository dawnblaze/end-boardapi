begin transaction
--start template
  alter table [Employee.Locator] alter column Locator varchar(1024) null
  alter table [Employee.Locator] add RawInput varchar(1024) not null
  alter table [Employee.Locator] add SortIndex smallint null
  alter table [Employee.Locator]
	 add constraint df_Employee_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Employee.Locator] alter column SortIndex smallint not null

  alter table [Employee.Locator] add IsValid bit null
  alter table [Employee.Locator]
	 add constraint df_Employee_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Employee.Locator] alter column IsValid bit not null

  alter table [Employee.Locator] add IsVerified bit null
  alter table [Employee.Locator]
	 add constraint df_Employee_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Employee.Locator] alter column IsVerified bit not null
--end template

--everything after this is copy/pasted and replaced with the table
  alter table [Business.Locator] alter column Locator varchar(1024) null
  alter table [Business.Locator] add RawInput varchar(1024) not null
  alter table [Business.Locator] add SortIndex smallint null
  alter table [Business.Locator]
	 add constraint df_Business_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Business.Locator] alter column SortIndex smallint not null

  alter table [Business.Locator] add IsValid bit null
  alter table [Business.Locator]
	 add constraint df_Business_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Business.Locator] alter column IsValid bit not null

  alter table [Business.Locator] add IsVerified bit null
  alter table [Business.Locator]
	 add constraint df_Business_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Business.Locator] alter column IsVerified bit not null

  
  alter table [Contact.Locator] alter column Locator varchar(1024) null
  alter table [Contact.Locator] add RawInput varchar(1024) not null
  alter table [Contact.Locator] add SortIndex smallint null
  alter table [Contact.Locator]
	 add constraint df_Contact_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Contact.Locator] alter column SortIndex smallint not null

  alter table [Contact.Locator] add IsValid bit null
  alter table [Contact.Locator]
	 add constraint df_Contact_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Contact.Locator] alter column IsValid bit not null

  alter table [Contact.Locator] add IsVerified bit null
  alter table [Contact.Locator]
	 add constraint df_Contact_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Contact.Locator] alter column IsVerified bit not null

  
  alter table [Lead.Locator] alter column Locator varchar(1024) null
  alter table [Lead.Locator] add RawInput varchar(1024) not null
  alter table [Lead.Locator] add SortIndex smallint null
  alter table [Lead.Locator]
	 add constraint df_Lead_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Lead.Locator] alter column SortIndex smallint not null

  alter table [Lead.Locator] add IsValid bit null
  alter table [Lead.Locator]
	 add constraint df_Lead_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Lead.Locator] alter column IsValid bit not null

  alter table [Lead.Locator] add IsVerified bit null
  alter table [Lead.Locator]
	 add constraint df_Lead_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Lead.Locator] alter column IsVerified bit not null

  
  alter table [Company.Locator] alter column Locator varchar(1024) null
  alter table [Company.Locator] add RawInput varchar(1024) not null
  alter table [Company.Locator] add SortIndex smallint null
  alter table [Company.Locator]
	 add constraint df_Company_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Company.Locator] alter column SortIndex smallint not null

  alter table [Company.Locator] add IsValid bit null
  alter table [Company.Locator]
	 add constraint df_Company_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Company.Locator] alter column IsValid bit not null

  alter table [Company.Locator] add IsVerified bit null
  alter table [Company.Locator]
	 add constraint df_Company_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Company.Locator] alter column IsVerified bit not null

  
  alter table [Business.Location.Locator] alter column Locator varchar(1024) null
  alter table [Business.Location.Locator] add RawInput varchar(1024) not null
  alter table [Business.Location.Locator] add SortIndex smallint null
  alter table [Business.Location.Locator]
	 add constraint df_Business_Location_Loc_sort_default
	 default (0) for [SortIndex]
  alter table [Business.Location.Locator] alter column SortIndex smallint not null

  alter table [Business.Location.Locator] add IsValid bit null
  alter table [Business.Location.Locator]
	 add constraint df_Business_Location_Loc_isvalid_default
	 default (0) for [IsValid]
  alter table [Business.Location.Locator] alter column IsValid bit not null

  alter table [Business.Location.Locator] add IsVerified bit null
  alter table [Business.Location.Locator]
	 add constraint df_Business_Location_isverified_sort_default
	 default (0) for [IsVerified]
  alter table [Business.Location.Locator] alter column IsVerified bit not null

  commit