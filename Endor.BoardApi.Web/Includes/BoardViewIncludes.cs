﻿using Endor.Api.Common;
using System.Collections.Generic;

namespace Endor.BoardApi.Web.Includes
{
    /// <summary>
    /// Child Includes for BoardViews
    /// </summary>
    public class BoardViewIncludes : IExpandIncludes
    {
        /// <summary>
        /// BoardDefinitions Include
        /// </summary>
        public IncludesLevel BoardDefinitionLevel { get; set; } = IncludesLevel.Full;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.BoardDefinitionLevel), new IncludeSettings(this.BoardDefinitionLevel) },
            };
        }
    }
}
