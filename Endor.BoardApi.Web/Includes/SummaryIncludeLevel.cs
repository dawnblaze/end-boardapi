﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BoardApi.Web.Includes
{
    /// <summary>
    /// Include level for summary queries
    /// </summary>
    public enum SummaryIncludeLevel
    {
        /// <summary>
        /// only favorites
        /// </summary>
        favorite,
        /// <summary>
        /// all
        /// </summary>
        all
    }
}
