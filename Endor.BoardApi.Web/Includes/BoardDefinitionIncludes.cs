﻿using Endor.Api.Common;
using System.Collections.Generic;

namespace Endor.BoardApi.Web.Includes
{
    /// <summary>
    /// Child Includes for BoardDefinitions
    /// </summary>
    public class BoardDefinitionIncludes : IExpandIncludes
    {
        /// <summary>
        /// Employee Links Include
        /// </summary>
        public IncludesLevel EmployeeLinks { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Board View Links Include
        /// </summary>
        public IncludesLevel BoardViewLinks { get; set; } = IncludesLevel.Full;
        /// <summary>
        /// Role Links Include
        /// </summary>
        public IncludesLevel RoleLinks { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Module Links Include
        /// </summary>
        public IncludesLevel ModuleLinks { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.EmployeeLinks), new IncludeSettings(this.EmployeeLinks) },
                { nameof(this.BoardViewLinks), new IncludeSettings(this.BoardViewLinks) },
                { nameof(this.RoleLinks), new IncludeSettings(this.RoleLinks) },
                { nameof(this.ModuleLinks), new IncludeSettings(this.ModuleLinks) },
            };
        }
    }
}
