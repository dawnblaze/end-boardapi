﻿using Endor.EF;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Endor.BoardApi.Web
{
    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : Endor.Api.Common.Startup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

        /// <summary>
        /// This method is overridden to specify the APIName
        /// </summary>
        /// <returns></returns>
        public override string GetAPIName()
        {
            return "Endor Board Api";
        }

        /// <summary>
        /// This method is overridden to specify the APIVersion
        /// </summary>
        /// <returns></returns>
        public override string GetAPIVersion()
        {
            return "v1.3.1";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public override void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<IMigrationHelper, MigrationHelper>();
            base.ConfigureExternalEndorServices(services);
        }

        /// <summary>
        /// Method that gets the configuration builder overridden to wire the specific Startup class in.
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public override IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = base.GetConfigurationBuilder(env);

            if (env.EnvironmentName == "Development")
            {
                builder.AddUserSecrets<Startup>();
            }

            return builder;
        }
    }
}
