﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;

namespace Endor.Api.Web.Annotation
{


    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class SwaggerCustomResponseAttribute : SwaggerResponseAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="type"></param>
        /// <param name="description"></param>
        public SwaggerCustomResponseAttribute(int statusCode, Type type = null, string description = null) : base(statusCode, description, type)
        {

        }


    }
}
