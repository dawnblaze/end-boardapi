﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response for a Business Location's status change attempt
    /// </summary>
    public class ChangeMultipleLocationStatusResponse : IGenericResponse
    {
        /// <summary>
        /// Location Ids
        /// </summary>
        public int[] Ids { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// Error message, if there is an error
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
