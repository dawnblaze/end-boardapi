﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response for an Entity's status change attempt
    /// </summary>
    public class ChangeEntityStatusResponse : IGenericResponse
    {
        /// <summary>
        /// Entity Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// Error message, if there is an error
        /// </summary>
        public string ErrorMessage { get; set; }

    }
}
