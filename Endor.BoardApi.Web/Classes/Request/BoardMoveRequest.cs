﻿namespace Endor.BoardApi.Web.Classes.Request
{
    /// <summary>
    /// request for board move
    /// </summary>
    public class BoardMoveRequest
    {
        /// <summary>
        /// property name board column
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// old value 
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// new value 
        /// </summary>
        public string To { get; set; }
    }
}
