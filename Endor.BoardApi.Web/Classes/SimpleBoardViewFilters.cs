﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Endor.Api.Common.Classes;
using Endor.Models;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// Simple Board View Filters for HTTP GET Methods
    /// </summary>
    public class SimpleBoardViewFilters : IQueryFilters<SimpleBoardView>
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// If supplied, filters for Boards with the specified DataType.
        /// </summary>
        public short? DataType { get; set; }

        /// <summary>
        /// Query Filter for filters on Board View
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleBoardView, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SimpleBoardView, bool>>>();

            if (this.IsActive.HasValue)
                predicates.Add(bdd => bdd.IsActive == this.IsActive);

            if (this.DataType.HasValue)
                predicates.Add(bdd => bdd.DataType == (DataType)this.DataType);

            // ModuleType, RoleID, and EmployeeID will need to be set up after their Links are created.

            return predicates.ToArray();
        }
    }
}
