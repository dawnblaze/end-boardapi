﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// simple tag object
    /// </summary>
    public class SimpleTag
    {
        /// <summary>
        /// ID of the tag
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Name of the tag
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// color of the tag
        /// </summary>
        public string Color { get; set; }
    }
}
