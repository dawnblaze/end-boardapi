﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Endor.Api.Common.Classes;
using Endor.Models;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// Board Definition Filters for HTTP GET Methods
    /// </summary>
    public class BoardDefinitionFilters : IQueryFilters<BoardDefinitionData>
    {
        /// <summary>
        /// When true, returns the list of active subscriptions for the calling employee (Defaults to True)
        /// <para>This should also include any Boards where IsAlwaysShown == true even if not subscribed.</para>
        /// </summary>
        public bool? MySubscriptions { get; set; }
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// If supplied, filters for Boards with the specified DataType.
        /// </summary>
        public short? DataType { get; set; }
        /// <summary>
        /// If specified, filters for Boards that apply to the specified module.
        /// </summary>
        public int? ModuleType { get; set; }
        /// <summary>
        /// If specified, filter for Boards that apply to the specified role.
        /// </summary>
        public int? RoleID { get; set; }
        /// <summary>
        /// If specified, filters for Boards that are valid for the specified employee (based on their role).
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Query Filter for filters on Board Definitions
        /// </summary>
        /// <returns></returns>
        public Expression<Func<BoardDefinitionData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<BoardDefinitionData, bool>>>
            {
                // this filter always exists
                bdd => bdd.IsAlwaysShown == true
            };

            // default to true
            if (!this.MySubscriptions.HasValue)
                this.MySubscriptions = true;

            if (this.IsActive.HasValue)
                predicates.Add(bdd => bdd.IsActive == this.IsActive);

            if (this.DataType.HasValue)
                predicates.Add(bdd => bdd.DataType == (DataType)this.DataType);

            // ModuleType, RoleID, and EmployeeID will need to be set up after their Links are created.

            return predicates.ToArray();
        }
    }
}
