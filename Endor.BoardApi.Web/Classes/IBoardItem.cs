﻿using Endor.Models;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// Interface Board Item
    /// </summary>
    public interface IBoardItem
    {
        /// <summary>
        /// The ID of the record represented by the card of list item.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// The date time the record was last modified.
        /// </summary>
        DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Class Type ID
        /// </summary>
        int ClassTypeID { get; set; }

        /// <summary>
        /// The Name of the record.
        /// </summary>
        string Name { get; set; }
    }

    /// <summary>
    /// Base Board Item
    /// </summary>
    public abstract class BaseBoardItem: IBoardItem
    {
        /// <summary>
        /// The ID of the record represented by the card of list item.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Class Type ID
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the record was last modified.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The Name of the record.
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// Order Item Board Item
    /// </summary>
    public class OrderItemBoardItem: BaseBoardItem
    {
        /// <summary>
        /// Transaction Type
        /// </summary>
        public OrderTransactionType TransactionType { get; set; }

        /// <summary>
        /// Order ID
        /// </summary>
        public int OrderID { get; set; }

        /// <summary>
        /// Order Number
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Estimate Number
        /// </summary>
        public string EstimateNumber { get; set; }

        /// <summary>
        /// Order Status ID
        /// </summary>
        public byte OrderStatusID { get; set; }

        /// <summary>
        /// Order Due Date
        /// </summary>
        public DateTime? OrderDueDate { get; set; }

        /// <summary>
        /// Line Item Status ID
        /// </summary>
        public short ItemStatusID { get; set; }

        /// <summary>
        /// Line Item Status Text
        /// </summary>
        public string ItemStatusText { get; set; }

        /// <summary>
        /// Line Item SubStatus ID
        /// </summary>
        public short? SubStatusID { get; set; }

        /// <summary>
        /// Line Item Substatus Text
        /// </summary>
        public string SubStatusText { get; set; }

        /// <summary>
        /// Line Item Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Line Item Due Date
        /// </summary>
        public DateTime? DueDT { get; set; }

        /// <summary>
        /// Company ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Line Item Number
        /// </summary>
        public short LineItemIndex { get; set; }

        /// <summary>
        /// Line Item Name
        /// </summary>
        public string LineItemName { get; set; }

        /// <summary>
        /// Order Line Item Count
        /// </summary>
        public short LineItemCount { get; set; }

        /// <summary>
        /// Salesperson ID
        /// </summary>
        public int? SalespersonID { get; set; }

        /// <summary>
        /// AssignedTo ID
        /// </summary>
        public int? AssignedToID { get; set; }

        /// <summary>
        /// Designer ID
        /// </summary>
        public int? DesignerID { get; set; }

        /// <summary>
        /// Salesperson Text
        /// </summary>
        public string SalespersonText { get; set; }

        /// <summary>
        /// AssignedTo Text
        /// </summary>
        public string AssignedToText { get; set; }

        /// <summary>
        /// AssignedTo's Initials from First and Last
        /// </summary>
        public string AssignedToInitials { get; set; }

        /// <summary>
        /// Designer Text
        /// </summary>
        public string DesignerText { get; set; }

        /// <summary>
        /// The total price of the line item.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// simple list of simple tags
        /// </summary>
        public List<SimpleTag> Tags { get; set; }

        /// <summary>
        /// The Priority of the record.  This is used for sorting.The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public float Priority { get; set; }

        /// <summary>
        /// Flag indicating if the record is urgent/rush.Urgent records sort ahead of non-urgent records.  The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public bool IsUrgent { get; set; }
    }

    /// <summary>
    /// Order Board Item
    /// </summary>
    public class OrderBoardItem : BaseBoardItem
    {
        /// <summary>
        /// Transaction Type
        /// </summary>
        public OrderTransactionType TransactionType { get; set; }

        /// <summary>
        /// Order Due Date
        /// </summary>
        public DateTime? DueDT { get; set; }

        /// <summary>
        /// AssignedTo ID
        /// </summary>
        public int? AssignedToID { get; set; }

        /// <summary>
        /// AssignedTo Text
        /// </summary>
        public string AssignedToText { get; set; }

        /// <summary>
        /// Salesperson ID
        /// </summary>
        public int? SalespersonID { get; set; }

        /// <summary>
        /// Salesperson ShortName
        /// </summary>
        public string SalespersonText { get; set; }

        /// <summary>
        /// Order Number
        /// </summary>
        public string OrderNumber { get; set; }        

        /// <summary>
        /// Order Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The name of the company's payment terms.
        /// </summary>
        public string PaymentTermsText { get; set; }

        /// <summary>
        /// Company ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// The status ID of the Order
        /// </summary>
        public int OrderStatusID { get; set; }

        /// <summary>
        /// Order Status Text
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// Total price of the Order
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The total balance remaining on the order.
        /// </summary>
        public decimal BalanceOutstanding { get; set; }

        /// <summary>
        /// An embedded JSON list of Destinations for this Order. (Not populated currently.)
        /// </summary>
        public string DestinationType { get; set; }

        /// <summary>
        /// The days the order is late, computed based on the status and payment terms of the order.  
        /// <para>For Orders in Pre-WIP, WIP, Built, Invoicing → this is NULL</para>
        /// <para>For Orders in Invoiced → this is computed based on the Current Date, the Invoiced Date, and the Payment Term rules for the customer.</para>
        /// <para>For Orders in Closed→ this is NULL.</para>
        /// </summary>
        public int? DaysLate { get; set; }

        /// <summary>
        /// The ID of the Location for this order.
        /// </summary>
        public int LocationID { get; set; }

        /// <summary>
        /// The name of the Location for this order.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The Priority of the record.  This is used for sorting.The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public float Priority { get; set; }

        /// <summary>
        /// Flag indicating if the record is urgent/rush.Urgent records sort ahead of non-urgent records.  The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public bool IsUrgent { get; set; }

        /// <summary>
        /// simple list of simple tags
        /// </summary>
        public List<SimpleTag> Tags { get; set; }

        /// <summary>
        /// Order Item Count
        /// </summary>
        public short OrderItemCount { get; set; }
        /// <summary>
        /// Production Days Late
        /// </summary>
        public int? ProductionDaysLate { get; internal set; }
    }

    /// <summary>
    /// Estimate Board Item
    /// </summary>
    public class EstimateBoardItem : BaseBoardItem
    {
        /// <summary>
        /// Transaction Type
        /// </summary>
        public OrderTransactionType TransactionType { get; set; }

        /// <summary>
        /// Order Due Date
        /// </summary>
        public DateTime? DueDT { get; set; }

        /// <summary>
        /// Salesperson ID
        /// </summary>
        public int? SalespersonID { get; set; }

        /// <summary>
        /// Salesperson ShortName
        /// </summary>
        public string Salesperson { get; set; }

        /// <summary>
        /// Estimate Number
        /// </summary>
        public string EstimateNumber { get; set; }

        /// <summary>
        /// Company ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// Company Name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// The status of the Estimate
        /// </summary>
        public int OrderStatusID { get; set; }

        /// <summary>
        /// Total price of the Estimate
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The ID of the Location for this estimate.
        /// </summary>
        public int LocationID { get; set; }

        /// <summary>
        /// The name of the Location for this estimate.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The Priority of the record.  This is used for sorting.The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public float Priority { get; set; }

        /// <summary>
        /// Flag indicating if the record is urgent/rush.Urgent records sort ahead of non-urgent records.  The normal sort is "IsUrgent DESC, Priority ASC".
        /// </summary>
        public bool IsUrgent { get; set; }
    }
    
    /// <summary>
         /// Company Board Item
         /// </summary>
    public class CompanyBoardItem : BaseBoardItem
    {
        /// <summary>
        /// The name of the company.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// The First and Last Name of the Salesperson.  If more than 1 salesperson is defined, only the first is returned.
        /// </summary>
        public string Salesperson { get; set; }

        /// <summary>
        /// The ID of the primary contact.  NULL if not assigned.
        /// </summary>
        public int? PrimaryContactID { get; set; }

        /// <summary>
        /// The First and Last Name of the primary contact if assigned.
        /// </summary>
        public string PrimaryContactText { get; set; }

        /// <summary>
        /// The ID of the default Location for this company.
        /// </summary>
        public int LocationID { get; set; }

        /// <summary>
        /// The ID of the Billing contact.  NULL if not assigned.
        /// </summary>
        public int? BillingContactID { get; set; }

        /// <summary>
        /// The First and Last Name of the Billing contact if assigned.
        /// </summary>
        public string BillingContactText { get; set; }

        /// <summary>
        /// The primary address for this company.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The short form of the timezone (as a string) for this company.
        /// </summary>
        public string TimeZone { get; set; }

        /// <summary>
        /// The primary phone number of the company record.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The ID of the status of this company (Lead, Prospect, Customer, Vendor, etc.).  This is a FLAG enum field.
        /// </summary>
        public int StatusID { get; set; }

        /// <summary>
        /// The name of the company's payment terms.
        /// </summary>
        public string PaymentTermsText { get; set; }
    }

    /// <summary>
    /// Contact Board Item
    /// </summary>
    public class ContactBoardItem : BaseBoardItem
    {

    }

    /// <summary>
    /// Destination Board Item
    /// </summary>
    public class DestinationBoardItem : BaseBoardItem
    {

    }

    /// <summary>
    /// Opportunity Board Item
    /// </summary>
    public class OpportunityBoardItem : BaseBoardItem
    {

    }

    /// <summary>
    /// Activity Board Item
    /// </summary>
    public class ActivityBoardItem : BaseBoardItem
    {

    }

}