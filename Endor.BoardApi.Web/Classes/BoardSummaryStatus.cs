﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// result of /api/board/summarylist
    /// </summary>
    public class BoardSummaryStatus
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// count?
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// query time
        /// </summary>
        public DateTime QueryDT { get; set; }
        /// <summary>
        /// isactive
        /// </summary>
        public bool IsActive { get; set; }
    }
}
