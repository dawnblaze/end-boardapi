﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.BoardApi.Web.Classes
{
    /// <summary>
    /// board data class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BoardData<T> where T: IBoardItem
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ID { get { return BoardID;  } }

        /// <summary>
        ///The ID of the board whose data is being generated.
        /// </summary>
        public int BoardID { get; set; }        

        ///The ID of the employee who the data was generated for.  Depending on the board conditions, the same board may return different results for different employees.	
        public int UserID { get; set; }

        /// <summary>
        /// The ClassTypeID of BoardData, which is always 14050.	
        /// </summary>
        public int ClassTypeID { get { return 14050; } set { } }

        /// <summary>
        ///An ClassTypeID of the data the board contains, which is the ClassTypeID of the BoardDataItems contained.
        /// </summary>
        public int AssociatedClassTypeID { get; set; }

        /// <summary>
        ///The date time the data was gathered.
        /// </summary>
        public DateTime CreatedDT { get; set; }

        /// <summary>
        /// The number of records returned in this collection.
        /// </summary>
        public int Records { get { return this.Data?.Count() ?? 0; } set { } }

        /// <summary>
        /// A list of IBoardItemData holding the data for the cards or list items.
        /// </summary>
        public List<T> Data { get; set; }
    }
}
