﻿using System;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Common;
using Endor.Api.Common.Classes;
using Endor.Api.Common.Controllers;
using Endor.Api.Web.Annotation;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Includes;
using Endor.BoardApi.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Endor.BoardApi.Web.Controllers
{
    /// <summary>
    /// Board Definitions Controller
    /// </summary>
    [Route("API/Board/Definition")]
    public class BoardDefinitionController : CRUDController<BoardDefinitionData, BoardDefinitionService, short>
    {
        /// <summary>
        /// API Endpoint for BoardDefinitions
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BoardDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient,
            ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        #region Obsolete Members

        /// <summary>
        /// Returns all of the Board Definitions
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        #endregion

        /// <summary>
        /// Returns a single Board Definition by ID
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardDefinitionData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            var includes = new BoardDefinitionIncludes()
            {
                BoardViewLinks = IncludesLevel.Full,
                EmployeeLinks = IncludesLevel.Full,
                ModuleLinks = IncludesLevel.Full,
                RoleLinks = IncludesLevel.Full,
            };
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Updates a single Board Definition by ID
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <param name="update">Updated Board Definition data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardDefinitionData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] BoardDefinitionData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Board Definition
        /// </summary>
        /// <param name="newModel">New Board Definition data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardDefinitionData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition creation fails")]
        public override async Task<IActionResult> Create([FromBody] BoardDefinitionData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }


        /// <summary>
        /// Clones an existing Board Definition to a new Board Definition
        /// </summary>
        /// <param name="ID">ID of the Board Definition to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/CloneObsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Board Definition does not exist or the cloning fails.")]
        [Obsolete]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Clones an existing Board Definition to a new Board Definition
        /// </summary>
        /// <param name="ID">ID of the Board Definition to clone from</param>
        /// <param name="newName">The new board name to use</param>"
        /// <returns></returns>
        [HttpPost("{ID}/Clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Board Definition does not exist or the cloning fails.")]
        public async Task<IActionResult> Clone(short ID, [FromQuery] string newName)
        {
            if (String.IsNullOrWhiteSpace(newName))
                return await base.Clone(ID);

            try
            {
                var resp = await this._service.CloneAsync(ID, newName, this.User.UserLinkID());
                if (resp != null)
                    return new OkObjectResult(resp);
                else
                    return new BadRequestResult();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("not found"))
                    return new NotFoundResult();
                else
                    return new BadRequestObjectResult(ex.Message);
            }
        }

        /// <summary>
        /// Deletes a Board Definition by ID
        /// </summary>
        /// <param name="ID">ID of the Board Definition to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets all Board Definitions for the specified Query Params
        /// </summary>
        /// <param name="filters">Board Definition Filters</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardDefinitionData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetBoardDefinitionsByParams([FromQuery] BoardDefinitionFilters filters)
        {
            return new OkObjectResult(await this._service.GetAllWithFilters(filters));
        }

        #region Board Definition Actions

        /// <summary>
        /// Sets a Board Definition to to Active
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Board Definition to Inactive
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Checks if a Board Definition can be deleted
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <returns></returns>
        [HttpGet("{ID}/Action/CanDelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Posts the time and record count from running the query against the board totals.
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <param name="RecordCount">The number of records returns in the query </param>
        /// <param name="TimeInSec">The time (in seconds) that the Board query took to run.</param>
        /// <returns></returns>
        [Route("{ID}/Action/PostResults")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist or fails to post results.")]
        public async Task<IActionResult> PostResults(short ID, int RecordCount, float TimeInSec)
        {
            var resp = await this._service.PostResults(ID, RecordCount, TimeInSec);
            return resp.ToResult();
        }

        /// <summary>
        /// Resets the cummulative run timers for the board.
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/ResetTimer")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist or fails to reset the timer.")]
        public async Task<IActionResult> ResetTimer(short ID)
        {
            var resp = await this._service.ResetTimer(ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Resets the cummulative run timers for all boards for the Business.
        /// </summary>
        /// <returns></returns>
        [Route("All/Action/ResetTimer")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If it fails to reset the timers.")]
        public async Task<IActionResult> ResetTimerForAll()
        {
            var resp = await this._service.ResetAllTimers();
            return resp.ToResult();
        }

        /// <summary>
        /// Links a board definition to a employee
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="EmployeeId">Employee Id</param>
        /// <param name="sortIndex">Sort Index</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkEmployee/{EmployeeId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkBoardEmployee(short ID, short EmployeeId, [FromQuery] byte? sortIndex = null)
        {
            var success = await this._service.LinkEmployeeBoard(ID, EmployeeId, sortIndex);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// UnLinks a board definition to a employee
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="EmployeeId">Employee Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/UnlinkEmployee/{EmployeeId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnLinkBoardEmployee(short ID, short EmployeeId)
        {
            var success = await this._service.UnLinkEmployeeBoard(ID, EmployeeId);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// Links a board definition to a role
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="RoleId">Role Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkRole/{RoleId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkBoardRole(short ID, short RoleId)
        {
            var success = await this._service.LinkBoardRole(ID, RoleId);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// UnLinks a board definition to a role
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="RoleId">Role Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/UnlinkRole/{RoleId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnLinkBoardRole(short ID, short RoleId)
        {
            var success = await this._service.UnLinkBoardRole(ID, RoleId);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// Links a board definition to a module
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="moduleType">Module Type</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkModule/{moduleType}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkBoardModule(short ID, Module moduleType)
        {
            var success = await this._service.LinkBoardModule(ID, moduleType);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// UnLinks a board definition to a module
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="moduleType">Module Type</param>
        /// <returns></returns>
        [Route("{ID}/Action/UnlinkModule/{moduleType}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnLinkBoardModule(short ID, Module moduleType)
        {
            var success = await this._service.UnLinkBoardModule(ID, moduleType);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }
            return BadRequest(success.Message);
        }

        /// <summary>
        /// Checks if a Board Definition is already a favorite
        /// </summary>
        /// <param name="ID">Board Definition ID</param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID. (Optional) If not provided uses requester's EmployeeID.</param>
        /// <returns></returns>
        [HttpGet("{ID}/Action/IsFavorite/Module/{moduleID}/{employeeID?}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> IsFavorite(short ID, Module moduleID, short? employeeID)
        {
            employeeID = employeeID ?? User.EmployeeID();
                        
            if (!employeeID.HasValue)
                return new BadRequestObjectResult("EmployeeID not specified or User is not an employee");

            var resp = await this._service.IsFavorite(ID, moduleID, employeeID.Value);
            if (resp.Success)
                return new OkObjectResult(resp);

            if (resp.Message.Contains("Not Found"))
                return new NotFoundResult();
            else
                return new BadRequestObjectResult(resp);
        }

        /// <summary>
        /// Favorite a board for the employee
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID. (Optional) If not provided uses requester's EmployeeID.</param>
        /// <returns></returns>
        [Route("{ID}/Action/Favorite/Module/{moduleID}/{employeeID?}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> FavoriteBoard(short ID, Module moduleID, short? employeeID)
        {
            employeeID = employeeID ?? User.EmployeeID();

            if (!employeeID.HasValue)
                return new BadRequestObjectResult("EmployeeID not specified or User is not an employee");

            var success = await this._service.FavoriteBoard(ID, moduleID, employeeID.Value);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// Favorite a board for the employee
        /// </summary>
        /// <param name="ID">Board Definition Id</param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID. (Optional) If not provided uses requester's EmployeeID.</param>
        /// <returns></returns>
        [Route("{ID}/Action/Unfavorite/Module/{moduleID}/{employeeID?}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnFavoriteBoard(short ID, Module moduleID, short? employeeID)
        {
            employeeID = employeeID ?? User.EmployeeID();

            if (!employeeID.HasValue)
                return new BadRequestObjectResult("EmployeeID not specified or User is not an employee");

            var success = await this._service.UnFavoriteBoard(ID, moduleID, employeeID.Value);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Board Definition Not Found");
            }

            return BadRequest(success.Message);
        }
        #endregion
    }
}
