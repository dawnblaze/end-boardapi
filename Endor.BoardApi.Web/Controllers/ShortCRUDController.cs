﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    ///                                                ICRUDController{M,S,I}
    ///  (OriginController{Origin,OriginService}) (ShortCRUDController{M,S}) (ShortCRUDInternalController{M,S}) (CRUDInternalController{M, S, I})
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="S"></typeparam>
    public class ShortCRUDController<M, S> : ShortCRUDInternalController<M, S>
        where M : class, IAtom<short>
        where S : AtomShortCRUDService<M>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        public ShortCRUDController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache) 
            : base(context, logger, rtmClient, taskQueuer, cache)
        {
        }

        [HttpGet]
        public virtual async Task<IActionResult> Read()
        {
            return await DoRead();
        }

        internal async Task<IActionResult> ReadWith(IExpandIncludes includes)
        {
            return await DoReadWith(includes);
        }

        internal async Task<IActionResult> FilteredReadWith(Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes)
        {
            return await DoFilteredReadWith(filterPredicate, includes);
        }

        internal async Task<IActionResult> ReadWith(short ID, IExpandIncludes includes)
        {
            return await DoReadWith(ID, includes);
        }

        [HttpGet("{ID}")]
        public virtual async Task<IActionResult> Read(short ID)
        {
            return await DoRead(ID);
        }

        [HttpPut("{ID}")]
        public virtual async Task<IActionResult> Update(short ID, [FromBody] M update)
        {
            return await DoUpdate(ID, update);
        }

        [HttpPost]
        public virtual async Task<IActionResult> Create([FromBody] M newModel, [FromQuery] Guid? tempID = null)
        {
            return await DoCreate(newModel, tempID);
        }

        [HttpPost("{ID}/clone")]
        public virtual async Task<IActionResult> Clone(short ID)
        {
            return await DoClone(ID);
        }

        [HttpDelete("{ID}")]
        public virtual async Task<IActionResult> Delete(short ID)
        {
            return await DoDelete(ID);
        }
    }
}
