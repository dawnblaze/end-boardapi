﻿using System;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Common.Classes;
using Endor.Api.Common.Controllers;
using Endor.Api.Common.Services;
using Endor.Api.Web.Annotation;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Includes;
using Endor.BoardApi.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Endor.BoardApi.Web.Controllers
{
    /// <summary>
    /// Board View Controller
    /// </summary>
    [Route("Api/Board/View")]
    public class BoardViewController : CRUDController<BoardView, BoardViewService, short>, ISimpleListableViewController<SimpleBoardView, short>
    {
        /// <summary>
        /// Listable service for Companies
        /// </summary>
        public ISimpleListableViewService<SimpleBoardView, short> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for BoardView
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BoardViewController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) 
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper) { }

        #region Overridden CRUD Methods

        #region Obsolete Members

        /// <summary>
        /// Returns all of the Board Views
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        #endregion

        /// <summary>
        /// Returns a single Board View by ID
        /// </summary>
        /// <param name="ID">Board View ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns all BoardViews and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">ID to retrieve</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes(int ID, [FromQuery] BoardViewIncludes includes)
        {
            return new OkObjectResult(await _service.GetWithIncludes(ID, includes));
        }

        /// <summary>
        /// Updates a single Board View by ID
        /// </summary>
        /// <param name="ID">Board View ID</param>
        /// <param name="update">Updated Board View data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board View does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] BoardView update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Board View
        /// </summary>
        /// <param name="newModel">New Board View data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board View creation fails")]
        public override async Task<IActionResult> Create([FromBody] BoardView newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Board View to a new Board View
        /// </summary>
        /// <param name="ID">ID of the Board View to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/Clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Board View does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a Board View by ID
        /// </summary>
        /// <param name="ID">ID of the Board View to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board View does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets all Board View for the specified Query Params
        /// </summary>
        /// <param name="filters">Board View Filters</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardView[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetBoardViewsByParams([FromQuery] BoardViewFilters filters)
        {
            return new OkObjectResult(await this._service.GetAllWithFilters(filters));
        }

        /// <summary>
        /// Gets a Simple list of Board Views
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleBoardView[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleBoardView[]> GetFilteredSimpleList([FromQuery] SimpleBoardViewFilters filters)
        {
            return await this.GetSimpleList<SimpleBoardView, short>(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a Simple list of Board Views
        /// </summary>
        /// <returns></returns>
        [HttpGet("obseletesimplelist")]
        [Obsolete]
        public async Task<SimpleBoardView[]> SimpleList()
        {
            return await this.GetFilteredSimpleList(null);
        }
        #region Board View Actions

        /// <summary>
        /// Sets a Board View to to Active
        /// </summary>
        /// <param name="ID">Board View ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board View does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Board View to Inactive
        /// </summary>
        /// <param name="ID">Board View Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Board View does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Checks if a Board View can be deleted
        /// </summary>
        /// <param name="ID">Board View Id</param>
        /// <returns></returns>
        [HttpGet("{ID}/Action/CanDelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Links a board to a board view
        /// </summary>
        /// <param name="ID">Board View Id</param>
        /// <param name="BoardId">Board Id</param>
        /// <param name="sortIndex">Sort Index</param>
        /// <returns></returns>
        [Route("{ID}/action/linkboard/{BoardId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkBoard(short ID, short BoardId, [FromQuery] byte? sortIndex = null)
        {
            (bool, string) success = await this._service.LinkBoard(ID, BoardId, sortIndex);

            if (success.Item1)
                return Ok();

            if (success.Item2.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(success.Item2);

            return BadRequest(success.Item2);
        }

        /// <summary>
        /// Unlinks a board to a board view
        /// </summary>
        /// <param name="ID">Board View Id</param>
        /// <param name="BoardId">Board Id</param>
        /// <returns></returns>
        [Route("{ID}/action/unlinkboard/{BoardId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Board View does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnlinkBoard(short ID, short BoardId)
        {
            (bool, string) success = await this._service.UnlinkBoard(ID, BoardId);

            if (success.Item1)
                return Ok();

            if (success.Item2.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(success.Item2);

            return BadRequest(success.Item2);
        }
        #endregion
    }
}
