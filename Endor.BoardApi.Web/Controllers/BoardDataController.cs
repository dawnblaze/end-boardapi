﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Services;
using Endor.BoardApi.Web.Includes;
using Endor.EF;
using Endor.Models;
using Endor.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.SwaggerUI;
using Endor.Api.Web.Annotation;

namespace Endor.BoardApi.Web.Controllers
{
    /// <summary>
    /// Board data Controller
    /// </summary>
    [Route("API/Board")]
    [Authorize]
    public class BoardDataController : Controller 
    {
        /// <summary>
        /// Lazy implementation of the service
        /// </summary>
        protected readonly Lazy<BoardDataService> _lazyService;
        /// <summary>
        /// Getter for the service
        /// </summary>
        protected BoardDataService _service { get { return _lazyService.Value; } }
        /// <summary>
        /// API Context
        /// </summary>
        private readonly ApiContext ctx;

        /// <summary>
        /// Api Endpoint for BoardView
        /// </summary>
        /// <param name="context">Api Context</param>
        public BoardDataController(ApiContext context)
        {
            ctx = context;
            this._lazyService = new Lazy<BoardDataService>(() => new BoardDataService(context, User.BID().Value));
        }

        /// <summary>
        /// Gets the Board Summary Status List
        /// </summary>
        /// <param name="module">Module</param>
        /// <param name="include">Summary Include Level</param>
        /// <returns></returns>
        [HttpGet("Summary")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardSummaryStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetSummaryList([FromQuery] int module, [FromQuery] SummaryIncludeLevel include = SummaryIncludeLevel.favorite)
        {
            var resp = await _service.GetSummaryList((Module)module, User.EmployeeID(), include);
            if (resp.Success)
                return new OkObjectResult(resp.Data);
            if (resp.ErrorMessage.Contains("Not Found"))
                return NotFound();
            if (resp.ErrorMessage.Contains("not authorized"))
                return Unauthorized();

            return new BadRequestObjectResult(resp.ErrorMessage);
        }

        /// <summary>
        /// Returns BoardData for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<IBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            var resp = await _service.GetBoardData(boardID, User.UserID().Value, take, skip, ids);
            if (resp.Success)
                return new OkObjectResult(resp.Data);
            if (resp.ErrorMessage.Contains("Not Found"))
                return NotFound();
            if (resp.ErrorMessage.Contains("not authorized"))
                return Unauthorized();

            return new BadRequestObjectResult(resp.ErrorMessage);
        }

        #region Additional DATA endpoints

        /// <summary>
        /// Returns Activity Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Activity")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<ActivityBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#warning This is not currently implemented. When it becomes implemented remember to remove this swagger response
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "This endpoint is not currently implemented.")]
        public async Task<IActionResult> GetActivityBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
#warning There currently is no DataType.Activity, this will need to be addressed
            return await Task.FromResult(this.StatusCode((int)HttpStatusCode.NotImplemented));
            /*
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Opportunity)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await GetData(boardID, take, skip, ids);
            */
        }

        /// <summary>
        /// Returns Company Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Company")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<CompanyBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetCompanyBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Company)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Contact Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Contact")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<ContactBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#warning This is not currently implemented. When it becomes implemented remember to remove this swagger response
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "This endpoint is not currently implemented.")]
        public async Task<IActionResult> GetContactBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Contact)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await Task.FromResult(this.StatusCode((int)HttpStatusCode.NotImplemented));
            //return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Destination Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Destination")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<DestinationBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#warning This is not currently implemented. When it becomes implemented remember to remove this swagger response
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "This endpoint is not currently implemented.")]
        public async Task<IActionResult> GetDestinationBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Destination)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await Task.FromResult(this.StatusCode((int)HttpStatusCode.NotImplemented));
            //return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Estimate Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Estimate")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<EstimateBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetEstimateBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Estimate)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Order Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Order")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<OrderBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetOrderBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Order)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Order Item Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/OrderItem")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<OrderItemBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetOrderItemBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.LineItem)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await GetData(boardID, take, skip, ids);
        }

        /// <summary>
        /// Returns Opportunity Board Data for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        [HttpGet("{boardID}/Data/Opportunity")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BoardData<OpportunityBoardItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Board Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Board Definition DataType does not match the route DataType")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#warning This is not currently implemented. When it becomes implemented remember to remove this swagger response
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "This endpoint is not currently implemented.")]
        public async Task<IActionResult> GetOpportunityBoardData([FromRoute] int boardID, [FromQuery] int take = 250, [FromQuery] int skip = 0, [FromQuery] int[] ids = null)
        {
            if (!(await this._service.CheckBoardRoute(boardID, DataType.Opportunity)))
                return new BadRequestObjectResult("Board DataType does not match route DataType.");

            return await Task.FromResult(this.StatusCode((int)HttpStatusCode.NotImplemented));
            //return await GetData(boardID, take, skip, ids);
        }

        #endregion
    }
}
