﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    public class ShortCRUDInternalController<M,S> : CRUDInternalController<M, S, short> 
        where M : class, IAtom<short>
        where S : AtomShortCRUDService<M>
    {
        public ShortCRUDInternalController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache) : base(context, logger, rtmClient, taskQueuer, cache)
        {
        }
    }
}
