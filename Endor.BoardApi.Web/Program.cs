﻿using Endor.Api.Common;


namespace Endor.BoardApi.Web
{
    /// <summary>
    /// Program
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            ProgramHelper<Endor.BoardApi.Web.Startup>.SetConfiguration("Endor.BoardApi");
        }
    }
}
