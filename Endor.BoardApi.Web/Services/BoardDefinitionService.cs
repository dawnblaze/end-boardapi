﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common;
using Endor.Api.Common.Classes;
using Endor.Api.Common.Services;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.BoardApi.Web.Services
{
    /// <summary>
    /// Board Definition Service
    /// </summary>
    public class BoardDefinitionService : AtomCRUDService<BoardDefinitionData, short>
    {
        private int? _userLinkId = null;
        private const string OptionKeyCopySettings = "BoardDefinition.Clone.CopySettings";
        private const string OptionKeyCopyDocuments = "BoardDefinition.Clone.CopyDocuments";

        /// <summary>
        /// Board Definition Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper"></param>
        public BoardDefinitionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "BoardViewLinks", "BoardViewLinks.BoardView", "ModuleLinks", "RoleLinks.Role", "EmployeeLinks" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<BoardDefinitionData> GetOldModelAsync(BoardDefinitionData newModel)
        {
            return await WherePrimary(ctx.BoardDefinitionData.IncludeAll(GetIncludes()).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Board Definition Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">ContactData</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(BoardDefinitionData newModel, Guid? tempGuid = null)
        {
            newModel.ID = await this.RequestIDAsync(this.BID, typeof(BoardDefinitionData));

            foreach(var child in newModel?.BoardViewLinks ?? Enumerable.Empty<BoardViewLink>())
            {
                child.BoardID = newModel.ID;
            }
            foreach (var child in newModel?.ModuleLinks ?? Enumerable.Empty<BoardModuleLink>())
            {
                child.BID = newModel.BID;
                child.BoardID = newModel.ID;
            }
            foreach (var child in newModel?.RoleLinks ?? Enumerable.Empty<BoardRoleLink>())
            {
                child.BID = newModel.BID;
                child.BoardID = newModel.ID;
            }
            await base.DoBeforeCreateAsync(newModel, tempGuid, getID: false);
        }

        /// <summary>
        /// Board Definition Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old BoardDefinition</param>
        /// <param name="updatedModel">Updated BoardDefinition</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(BoardDefinitionData oldModel, BoardDefinitionData updatedModel)
        {
            //set IDs and null out boardDefinitions
            foreach (var child in updatedModel?.ModuleLinks ?? Enumerable.Empty<BoardModuleLink>())
            { 
                child.BID = updatedModel.BID;
                child.BoardID = updatedModel.ID;
                child.BoardDefinition = null;
            }
            foreach (var child in oldModel?.ModuleLinks ?? Enumerable.Empty<BoardModuleLink>())
            {
                child.BoardDefinition = null;
            }

            //figure out which links are new and which are old
            bool hasNewModLinks = updatedModel.ModuleLinks != null;
            bool hasExistingModLinks = oldModel.ModuleLinks != null;
            IEnumerable<BoardModuleLink> deletedLinks = null;
            IEnumerable<BoardModuleLink> addedLinks = null;
            if (hasExistingModLinks && hasNewModLinks)
            {
                //deleted links exist in the old but not the new
                deletedLinks = oldModel.ModuleLinks.Where(x => updatedModel.ModuleLinks.FirstOrDefault(y => y.ModuleType == x.ModuleType) == null);
                //added links exist in the new but not the old
                addedLinks = updatedModel.ModuleLinks.Where(x => oldModel.ModuleLinks.FirstOrDefault(y => y.ModuleType == x.ModuleType) == null);
            }
            else if (hasExistingModLinks && !hasNewModLinks)
                deletedLinks = oldModel.ModuleLinks;
            else if (!hasExistingModLinks && hasNewModLinks)
                addedLinks = updatedModel.ModuleLinks;

            if (deletedLinks != null)
            {
                ctx.BoardModuleLink.RemoveRange(deletedLinks);
            }
            if (addedLinks != null)
            {
                ctx.BoardModuleLink.AddRange(addedLinks);
            }

            //set IDs and null out boardDefinitions
            foreach (var child in updatedModel?.RoleLinks ?? Enumerable.Empty<BoardRoleLink>())
            {
                child.BID = updatedModel.BID;
                child.BoardID = updatedModel.ID;
                child.BoardDefinition = null;
            }
            foreach (var child in oldModel?.RoleLinks ?? Enumerable.Empty<BoardRoleLink>())
            {
                child.BoardDefinition = null;
            }

            //figure out which links are new and which are old
            bool hasNewRoleLinks = updatedModel.RoleLinks != null;
            bool hasExistingRoleLinks = oldModel.RoleLinks != null;
            IEnumerable<BoardRoleLink> deletedRoleLinks = null;
            IEnumerable<BoardRoleLink> addedRoleLinks = null;
            if (hasExistingRoleLinks && hasNewRoleLinks)
            {
                //deleted links exist in the old but not the new
                deletedRoleLinks = oldModel.RoleLinks.Where(x => updatedModel.RoleLinks.FirstOrDefault(y => y.RoleID == x.RoleID) == null);
                //added links exist in the new but not the old
                addedRoleLinks = updatedModel.RoleLinks.Where(x => oldModel.RoleLinks.FirstOrDefault(y => y.RoleID == x.RoleID) == null);
            }
            else if (hasExistingRoleLinks && !hasNewRoleLinks)
                deletedRoleLinks = oldModel.RoleLinks;
            else if (!hasExistingRoleLinks && hasNewRoleLinks)
                addedRoleLinks = updatedModel.RoleLinks;

            if (deletedRoleLinks != null)
            {
                ctx.BoardRoleLink.RemoveRange(deletedRoleLinks);
            }
            if (addedRoleLinks != null)
            {
                ctx.BoardRoleLink.AddRange(addedRoleLinks);
            }

            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// before delete hook
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(BoardDefinitionData model)
        {
            ctx.BoardModuleLink.RemoveRange(model.ModuleLinks);
            ctx.BoardRoleLink.RemoveRange(model.RoleLinks);
            ctx.BoardEmployeeLink.RemoveRange(model.EmployeeLinks);

            return base.DoBeforeDeleteAsync(model);
        }

        ///// <summary>
        ///// Board Definition Service Override of DoBeforeCloneAsync
        ///// </summary>
        ///// <param name="clone">Cloned ContactData</param>
        ///// <returns></returns>
        //public override async Task DoBeforeCloneAsync(BoardDefinitionData clone)
        //{
        //    clone.Name = clone.Name + " (Clone)";
        //    await base.DoBeforeCloneAsync(clone);
        //}

        /// <summary>
        /// before clone hook
        /// makes sure name has cloned appended
        /// and doesn't conflict with an existing record
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(BoardDefinitionData clone)
        {
            clone.EmployeeLinks = null; //do not clone employeeLinks

            if (clone.RoleLinks != null)
            {
                foreach (var roleLink in clone.RoleLinks)
                {
                    roleLink.BID = this.BID;
                    roleLink.BoardID = 0;
                    roleLink.Role = null;//prevent clone from inserting new role
                    roleLink.BoardDefinition = null;//prevent clone from using old board entity
                }
            }

            if(clone.ModuleLinks != null)
            {
                foreach (var modLink in clone.ModuleLinks)
                {
                    modLink.BID = this.BID;
                    modLink.BoardID = 0;
                    modLink.BoardDefinition = null;
                }
            }

            var existing = await this.ctx.BoardDefinitionData.Where(l => l.BID == this.BID && l.Name.ToLower() == clone.Name.ToLower()).FirstOrDefaultAsync();
            if (existing != null)
            {
                var originalName = clone.Name;
                const string cloneIdentifier = " (Clone)";
                string currentCloneIdentifier = cloneIdentifier;

                //Ensure record doesn't already exist
                while (await this.ctx.BoardDefinitionData.WherePrimary(this.BID, x => x.Name.ToLower() == $"{originalName}{currentCloneIdentifier}".ToLower()).FirstOrDefaultAsync() != null)
                {
                    //Record already exists, append clone identifier until it doesn't
                    currentCloneIdentifier += cloneIdentifier;
                }

                //Modify Values
                clone.Name += currentCloneIdentifier;
            }

            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Potentially Copy Settings and Documents after BoardDefinitionData Clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        protected override async Task DoAfterCloneAsync(short clonedFromID, BoardDefinitionData clone)
       {
            OptionValue copySettings = (await base.GetUserOptionValueByName(OptionKeyCopySettings, this._userLinkId)).Value;
            OptionValue copyDocuments = (await base.GetUserOptionValueByName(OptionKeyCopyDocuments, this._userLinkId)).Value;

            if (copySettings == null || bool.TryParse(copySettings.Value, out bool copySettingsValue) && copySettingsValue)
            {//Copy if setting is null
                await CopyOptionValues(clonedFromID, clone.ID);
            }

            if (copyDocuments != null && bool.TryParse(copyDocuments.Value, out bool copyDocumentsValue) && copyDocumentsValue)
            {//Don't copy if setting is null
                DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.BoardDefinition, BucketRequest.Documents);
                await client.CloneAllDocumentsAsync(clone.ID);
            }

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Copy all Options from one BoardDefinitionData to another BoardDefinitionData
        /// </summary>
        /// <param name="clonedFromBoardDefinitionDataID"></param>
        /// <param name="newBoardDefinitionDataID"></param>
        /// <returns></returns>
        private async Task<PutOptionValuesResponse> CopyOptionValues(short clonedFromBoardDefinitionDataID, short newBoardDefinitionDataID)
        {
            GetOptionValuesResponse values = await _optionService.Get(null, this.BID, clonedFromBoardDefinitionDataID, null, null, null, null, 5 /*Operations*/);

            var optionValues = new List<Options>();

            if (values.Values != null)
            {
                optionValues = values.Values
                .Select(o => new Options
                {
                    OptionID = o.OptionID,
                    OptionName = o.Name,
                    Value = o.Value
                }).ToList();
            }
                
                
            PutOptionValuesResponse response = await _optionService.Put(optionValues, null, this.BID, newBoardDefinitionDataID, null, null, null, null);

            return response;
        }

        /// <summary>
        /// Clones a BoardDefinitionData from a given ID
        /// </summary>
        /// <param name="ID">ID of the BoardDefinition to clone</param>
        /// <param name="newName">New BoardDefinition Name</param>
        /// <param name="userLinkId"></param>
        /// <returns></returns>
        public async Task<BoardDefinitionData> CloneAsync(short ID, string newName, int? userLinkId)
        {
            try
            {
                this._userLinkId = userLinkId;
                var clone = await this.GetClone(ID);

                clone.Name = newName;

                await DoBeforeCloneAsync(clone);

                var cloneAfterSave = await this.CreateAsync(clone, null);

                await DoAfterCloneAsync(ID, cloneAfterSave);

                return cloneAfterSave;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return null;
            }
        }

        /// <summary>
        /// Get Child Service Associations for BoardDefinitionData
        /// </summary>
        /// <returns></returns>
        public override IChildServiceAssociation<BoardDefinitionData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<BoardDefinitionData, short>[]
            {
                new ChildLinkServiceAssociation<BoardViewLinkService, BoardDefinitionData, short, BoardViewLink>(this.ctx, this.logger, this.rtmClient, this.taskQueuer, this.cache, this, (board) => board.BoardViewLinks, this.migrationHelper)
                //CreateChildLinkAssociation<BoardViewLinkService, BoardViewLink>((board) => board.BoardViewLinks),
                //CreateChildLinkAssociation<BoardViewLink, BoardView>((board) => board.BoardViewLinks.Where(b => b.BID == this.BID).Select(b => b.BoardView).ToList())
            };
        }

        /// <summary>
        /// Lookup BoardDefinitionData by query and ID - Override
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">BoardDefinitionData ID</param>
        /// <returns></returns>
        public override IQueryable<BoardDefinitionData> WherePrimary(IQueryable<BoardDefinitionData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Additional logic to before before validation
        /// </summary>
        /// <param name="newModel">BoardDefinitionData model</param>
        public override void DoBeforeValidate(BoardDefinitionData newModel)
        {
            return;
        }

        /// <summary>
        /// Gets a list of Board Definitions for the specified filters
        /// </summary>
        /// <param name="filters">Board Definition Filters</param>
        /// <returns></returns>
        public async Task<List<BoardDefinitionData>> GetAllWithFilters(BoardDefinitionFilters filters)
        {
            return await ctx.Set<BoardDefinitionData>().IncludeAll(GetIncludes())
                .Where(bdd => bdd.BID == BID)
                .WhereAll(filters.WherePredicates())
                .ToListAsync();
        }

        /// <summary>
        /// Posts the time and record count from running the query against the board totals.
        /// </summary>
        /// <param name="id">Board Definition ID</param>
        /// <param name="recordCount">The number of records returns in the query </param>
        /// <param name="timeInSec">The time (in seconds) that the Board query took to run.</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> PostResults(short id, int recordCount, float timeInSec)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@BoardID", id),
                    new SqlParameter("@RecordCount", recordCount),
                    new SqlParameter("@TimeInSec", timeInSec),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Board.Definition.Action.PostResults] @BID, @BoardID, @RecordCount, @TimeInSec;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(id);

                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "Successfully Post Results.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Resets the cummulative run timers for the board.
        /// </summary>
        /// <param name="id">Board Definition ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> ResetTimer(short id)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@BoardID", id),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Board.Definition.Action.ResetTimer] @BID, @BoardID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(id);

                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "Successfully reset timer.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Resets the cummulative run timers for all boards for the Business.
        /// </summary>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> ResetAllTimers()
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@BoardID", DBNull.Value),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Board.Definition.Action.ResetTimer] @BID, @BoardID;", parameters: myParams);
                if (rowResult > 0)
                {
                    //await QueueIndexForClassType();

                    return new EntitiesActionChangeResponse
                    {
                        Message = "Successfully reset timers.",
                        Success = true
                    };
                }
                else
                {
                    return new EntitiesActionChangeResponse
                    {
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };

            }
        }

        /// <summary>
        /// Links a board definition to a employee
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="EmployeeId"></param>
        /// <param name="sortIndex"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> LinkEmployeeBoard(short ID, short EmployeeId, byte? sortIndex)
        {
            //Check that Board Definition Exists
            var bd = ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);

            if (bd==null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var entries = ctx.BoardEmployeeLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.EmployeeID == EmployeeId);
            var boardsForEmployee = ctx.BoardEmployeeLink.Where(x => x.BID == this.BID && x.EmployeeID == EmployeeId);
            if (entries.Count() >= 1)
            {
                var entry = entries.First();
                var sortIndexes = boardsForEmployee.Select(x => x.SortIndex);

                if (sortIndex.HasValue)
                {
                    var sortInt = sortIndex.Value;
                    if (!sortIndexes.Contains(sortInt))
                    {
                        //the value doesn't exist so insert it
                        entry.SortIndex = sortIndex.Value;
                    }
                    else
                    {
                        //increment values
                        var shifters = await boardsForEmployee.Where(t => t.SortIndex >= sortInt).ToListAsync();
                        shifters.ForEach(t => t.SortIndex += 1);
                        entry.SortIndex = sortInt;
                    }
                }
                else
                {
                    //insert at the end
                    entry.SortIndex = (short)(sortIndexes.Max() + 1);
                }
            }
            else
            {
                short tmpSortIndex = -1;
                if (sortIndex.HasValue)
                {
                    tmpSortIndex = sortIndex.Value;
                    //increment values
                    var shifters = await boardsForEmployee.Where(t => t.SortIndex >= tmpSortIndex).ToListAsync();
                    shifters.ForEach(t => t.SortIndex += 1);
                }
                else
                {
                    if (boardsForEmployee.Count() >= 1)
                        tmpSortIndex = (short)(boardsForEmployee.Select(x => x.SortIndex).Max() + 1);
                    else tmpSortIndex = 1;
                }
                var boardModuleLink = ctx.BoardModuleLink?.FirstOrDefault(x => x.BID == this.BID && x.BoardID == ID);
                BoardEmployeeLink boardEmployeeLink = new BoardEmployeeLink
                {
                    BID = this.BID,
                    BoardID = ID,
                    EmployeeID = EmployeeId,
                    SortIndex = tmpSortIndex,
                    ModuleType = boardModuleLink?.ModuleType ?? Module.Management
                };
                ctx.BoardEmployeeLink.Add(boardEmployeeLink);

            }
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully linked board",
                Success = true
            };
        }

        /// <summary>
        /// UnLinks a board definition to a employee
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="EmployeeId"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> UnLinkEmployeeBoard(short ID, short EmployeeId)
        {
            //Check that Board Definition Exists
            var bd = ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);
            
            if (bd == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }
            
            //check if it already exists
            var entries = ctx.BoardEmployeeLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.EmployeeID == EmployeeId);
            ctx.RemoveRange(entries);
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully unlinked board",
                Success = true
            };
        }

        /// <summary>
        /// Links a board definition to a role
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> LinkBoardRole(short ID, short RoleID)
        {
            //Check that Board Definition Exists
            var bd = ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);

            if (bd == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var entries = ctx.BoardRoleLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.RoleID == RoleID);
            if (entries.Count() >= 1)
            {
                //do nothing
            }
            else
            {
                
                BoardRoleLink boardRoleLink = new BoardRoleLink
                {
                    BID = this.BID,
                    BoardID = ID,
                    RoleID = RoleID
                };
                ctx.BoardRoleLink.Add(boardRoleLink);

            }
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully linked board to role",
                Success = true
            };
        }

        /// <summary>
        /// UnLinks a board definition to a role
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> UnLinkBoardRole(short ID, short RoleID)
        {
            //Check that Board Definition Exists
            var bd = ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);

            if (bd == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var entries = ctx.BoardRoleLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.RoleID == RoleID);
            ctx.RemoveRange(entries);
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully unlinked board",
                Success = true
            };
        }

        /// <summary>
        /// Links a board definition to a module
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="moduleType"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> LinkBoardModule(short ID, Module moduleType)
        {
            //Check that Board Definition Exists
            var bd = this.ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);

            if (bd == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var entries = ctx.BoardModuleLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.ModuleType == moduleType);
            if (entries.Count() >= 1)
            {
                //do nothing
            }
            else
            {

                BoardModuleLink boardModuleLink = new BoardModuleLink
                {
                    BID = this.BID,
                    BoardID = ID,
                    ModuleType = moduleType
                };
                ctx.BoardModuleLink.Add(boardModuleLink);

            }
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully linked board to module",
                Success = true
            };
        }

        /// <summary>
        /// UnLinks a board definition to a module
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="moduleType"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> UnLinkBoardModule(short ID, Module moduleType)
        {
            //Check that Board Definition Exists
            var bd = this.ctx.BoardDefinitionData.FirstOrDefaultPrimary(this.BID, ID);

            if (bd == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var entries = ctx.BoardModuleLink.Where(x => x.BID == this.BID && x.BoardID == ID && x.ModuleType == moduleType);
            ctx.RemoveRange(entries);
            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(bd.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully unlinked board",
                Success = true
            };
        }

        /// <summary>
        /// Checks if the board is a favorite of the employee
        /// </summary>
        /// <param name="id">Board Definition ID</param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<BooleanResponse> IsFavorite(short id, Module moduleID, short employeeID)
        {
            var board = await ctx.BoardDefinitionData.Where(b => b.BID == BID && b.ID == id)
                                    .Include("EmployeeLinks").FirstOrDefaultAsync();

            if (board == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    Message = "Board Definition Not Found"
                };
            }

            if (board.EmployeeLinks != null && board.EmployeeLinks.Count > 0)
            {
                var link = board.EmployeeLinks.Where(el => el.EmployeeID == employeeID && el.ModuleType == moduleID).FirstOrDefault();
                if ((link == null)||!(link.IsFavorite.HasValue && link.IsFavorite.Value))
                {
                    return new BooleanResponse()
                    {
                        Success = true,
                        Value = false,
                        Message = "Board is not a favorite."
                    };
                }
                else
                {
                    return new BooleanResponse()
                    {
                        Success = true,
                        Value = true,
                        Message = "Board is a favorite."
                    };
                }
            }
            else
            {
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Board is not a favorite."
                };
            }
        }

        /// <summary>
        /// Mark the board as a favorite of the employee
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> FavoriteBoard(short ID, Module moduleID, short employeeID)
        {
            //Check that Board Definition Exists
            var board = await ctx.BoardDefinitionData.Where(b => b.BID == BID && b.ID == ID)
                                    .Include("EmployeeLinks").FirstOrDefaultAsync();

            if (board == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            bool exists = false;
            if (board.EmployeeLinks != null && board.EmployeeLinks.Count > 0)
            {

                var boardEmployeeLink = board.EmployeeLinks.Where(el => el.EmployeeID == employeeID && el.ModuleType == moduleID).FirstOrDefault();
                if (boardEmployeeLink != null)
                {
                    exists = true;
                    if (boardEmployeeLink.IsFavorite.HasValue && boardEmployeeLink.IsFavorite.Value)
                    {
                        //do nothing
                    }
                    else
                    {
                        boardEmployeeLink.IsFavorite = true;
                        ctx.BoardEmployeeLink.Update(boardEmployeeLink);
                    }
                }
            }
            if (!exists)
            {
                var boardEmployeeLink = new BoardEmployeeLink
                {
                    BID = this.BID,
                    BoardID = ID,
                    EmployeeID = employeeID,
                    SortIndex = 0,
                    IsFavorite = true,
                    ModuleType = moduleID
                };
                ctx.BoardEmployeeLink.Add(boardEmployeeLink);
            }

            List<RefreshEntity> refreshes = DoGetRefreshMessagesOnUpdate(board);
            await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });

            var save = ctx.SaveChanges();
            if (save > 0)
            {
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                await base.QueueIndexForModel(board.ID);

                return new EntitiesActionChangeResponse
                {
                    Message = "Successfully set board to favorite",
                    Success = true
                };
            }
            else
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Nothing set to favorite",
                    Success = true
                };
            }
        }

        /// <summary>
        /// Un-Mark the board as a favorite of the employee
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="moduleID">Module ID</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> UnFavoriteBoard(short ID, Module moduleID, short employeeID)
        {
            //Check that Board Definition Exists
            var board = await ctx.BoardDefinitionData.Where(b => b.BID == BID && b.ID == ID)
                                    .Include("EmployeeLinks").FirstOrDefaultAsync();

            if (board == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Board Definition Not Found",
                    Success = false
                };
            }

            //check if it already exists
            if (board.EmployeeLinks != null && board.EmployeeLinks.Count > 0)
            {

                var boardEmployeeLinks = board.EmployeeLinks.Where(el => el.EmployeeID == employeeID && el.ModuleType == moduleID && el.IsFavorite.HasValue && el.IsFavorite.Value);
                if (boardEmployeeLinks != null)
                {
                    boardEmployeeLinks.ToList().ForEach(el => el.IsFavorite = false);
                    ctx.BoardEmployeeLink.UpdateRange(boardEmployeeLinks);
                }
            }
            List<RefreshEntity> refreshes = DoGetRefreshMessagesOnUpdate(board);
            await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });

            var save = ctx.SaveChanges();
            if (save > 0)
            {
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                await base.QueueIndexForModel(board.ID);

                return new EntitiesActionChangeResponse
                {
                    Message = "Successfully set board to unfavorite",
                    Success = true
                };
            } else
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Nothing set to unfavorite",
                    Success = true
                };
            }
        }

    }
}
