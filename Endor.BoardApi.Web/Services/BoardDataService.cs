﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.AEL;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Includes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Xml;
using Endor.Api.Common.Classes;

namespace Endor.BoardApi.Web.Services
{
    /// <summary>
    /// Board Data Service
    /// </summary>
    public class BoardDataService
    {
        private class AELDataProvider : IAELDataProvider
        {
            private readonly ApiContext _ApiContext;

            public AELDataProvider(short bid, ApiContext ctx)
            {
                this.BID = bid;
                this._ApiContext = ctx;
            }

            public short BID { get; private set; }

            public IQueryable<T> GetData<T>() where T : class, IAtom
            {
                return this._ApiContext.Set<T>().Where(x => x.BID.Equals(this.BID));
            }
        }

        /// <summary>
        /// API Context
        /// </summary>
        protected readonly ApiContext ctx;
        /// <summary>
        /// Business ID
        /// </summary>
        protected short BID;

        /// <summary>
        /// Board Data Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        public BoardDataService(ApiContext context, short bid)
        {
            this.ctx = context;
            this.BID = bid;
        }

        /// <summary>
        /// Gets the Board Summary Status List
        /// </summary>
        /// <param name="module">Module</param>
        /// <param name="employeeID">Employee ID</param>
        /// <param name="include">Summary Include Level</param>
        /// <returns></returns>
        public async Task<GenericResponse<List<BoardSummaryStatus>>> GetSummaryList(Module module, short? employeeID, SummaryIncludeLevel include = SummaryIncludeLevel.favorite)
        {
            if (!employeeID.HasValue)
            {
                return new GenericResponse<List<BoardSummaryStatus>>()
                {
                    Success = true,
                    Data = new List<BoardSummaryStatus>()
                };
            }

            int empID = (int)employeeID.Value;

            var boardDefs = await ctx.BoardDefinitionData.Include(m => m.EmployeeLinks)
                .Where(x => x.BID == BID && x.IsActive && x.EmployeeLinks.Any(m => m.ModuleType == module && m.IsFavorite == true)).ToListAsync();

            if (include == SummaryIncludeLevel.all)
            {
                boardDefs = await ctx.BoardDefinitionData.Include(m => m.ModuleLinks)
                    .Where(x => x.BID == BID && x.IsActive && x.ModuleLinks.Any(m => m.ModuleType == module)).ToListAsync();
            }

            var queryDT = DateTime.UtcNow;
            var summaryList = boardDefs.Select((x, index) => new BoardSummaryStatus()
            {
                ID = x.ID,
                Name = x.Name,
                IsActive = x.IsActive,
                QueryDT = queryDT,
                Count = x.LastRecordCount ?? 0
            }).ToList();

            return new GenericResponse<List<BoardSummaryStatus>>()
            {
                Success = true,
                Data = summaryList
            };
        }

        /// <summary>
        /// Returns BoardData for the given Board ID
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="userID">Calling User's ID</param>
        /// <param name="take">Amount of records to return</param>
        /// <param name="skip">Amount of records to skip before returning records</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        public async Task<GenericResponse<BoardData<IBoardItem>>> GetBoardData(int boardID, int userID, int take = 250, int skip = 0, int[] ids = null)
        {
            var boardDefinition = await ctx.BoardDefinitionData.Include(b => b.RoleLinks)
                .Where(b => b.BID == BID && b.ID == boardID).FirstOrDefaultAsync();

            if (boardDefinition == null)
                return new GenericResponse<BoardData<IBoardItem>>()
                {
                    Success = false,
                    Message = "Board Definition Not Found",
                    ErrorMessage = "Board Definition Not Found"
                };

            if (!(await IsUserAuthorizedForBoard(userID, boardDefinition)))
                return new GenericResponse<BoardData<IBoardItem>>()
                {
                    Success = false,
                    Message = "User is not authorized to view this board",
                    ErrorMessage = "User is not authorized to view this board"
                };

            try
            {
                List<IBoardItem> resp = null;
                switch (boardDefinition.DataType)
                {
                    case DataType.Order:
                        resp = GetBoardData(boardDefinition, GetOrderBoardData, userID, take, skip, ids);
                        break;
                    case DataType.Estimate:
                        resp = GetBoardData(boardDefinition, GetEstimateBoardData, userID, take, skip, ids);
                        break;
                    case DataType.LineItem:
                        resp = GetBoardData(boardDefinition, GetLineItemBoardData, userID, take, skip, ids);
                        break;
                    case DataType.Company:
                      resp = GetBoardData(boardDefinition, GetCompanyBoardData, userID, take, skip, ids);
                        break;
                    default:
                        return new GenericResponse<BoardData<IBoardItem>>()
                        {
                            Success = false,
                            Message = "Unsupported DataType",
                            ErrorMessage = "Unsupported DataType"
                        };
                }

                boardDefinition.LastRecordCount = resp?.Count() ?? 0;
                ctx.BoardDefinitionData.Update(boardDefinition);
                await ctx.SaveChangesAsync();

                return new GenericResponse<BoardData<IBoardItem>>()
                {
                    Success = true,
                    Data = new BoardData<IBoardItem>()
                    {
                        BID = BID,
                        BoardID = boardDefinition.ID,
                        UserID = userID,
                        AssociatedClassTypeID = (int)boardDefinition.DataType,
                        CreatedDT = DateTime.UtcNow,
                        Data = resp
                    }
                };
            }
            catch (Exception ex)
            {
                return new GenericResponse<BoardData<IBoardItem>>()
                {
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };

            }
        }

        /// <summary>
        /// Checks if the Board's DataType matches the Route's DataType
        /// </summary>
        /// <param name="boardID">Board ID</param>
        /// <param name="routeType">Route DataType</param>
        /// <returns></returns>
        public async Task<bool> CheckBoardRoute(int boardID, DataType routeType)
        {
            return await ctx.BoardDefinitionData
                            .AnyAsync(b => b.BID == BID && b.ID == boardID && b.DataType == routeType);
        }

        #region Private methods

        /// <summary>
        /// Gets Board Data
        /// </summary>
        /// <param name="boardDefinition"></param>
        /// <param name="DataGetter"></param>
        /// <param name="userID"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private List<IBoardItem> GetBoardData(BoardDefinitionData boardDefinition, Func<AELOperation, int[], IQueryable<IBoardItem>> DataGetter, int userID, int take = 250, int skip = 0, int[] ids = null)
        {
            AELOperation aelOp = AELOperation.Deserialize(boardDefinition.ConditionFx);

            ids = ids ?? new int[] { };

            var records = DataGetter(aelOp, ids)
                .Skip(skip)
                .Take(take)
            ;

            return records.ToList();
        }

        /// <summary>
        /// Creates an OrderBoardItem from an OrderData
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        static private OrderBoardItem GetOrderBoardItem(OrderData order)
        {
            #region Days Late
            int ? daysLate;

            if (order.OrderStatusID != OrderOrderStatus.OrderInvoiced)
                daysLate = 0;
            else if (order.Company?.DefaultPaymentTerms == null)
                daysLate = null;
            else if (order.Dates == null)
                daysLate = null;
            else
            {
                DateTime? keyDate = order.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Invoiced).Max(d => d.KeyDate);

                if (!keyDate.HasValue)
                    daysLate = null;
                else
                {
                    daysLate = (DateTime.UtcNow - keyDate.Value).Days - order.Company.DefaultPaymentTerms.DaysDue;

                    if (daysLate < 0)
                        daysLate = 0;
                }
            }

            #endregion Days Late

            #region Roles
            OrderEmployeeRole salespersonRole = order.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 1);
            OrderEmployeeRole assignedToRole = order.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 253);
            #endregion Roles

            return new OrderBoardItem()
            {
                ID = order.ID,
                ModifiedDT = order.ModifiedDT,
                OrderNumber = order.FormattedNumber ?? null,
                OrderStatusID = (byte)order.OrderStatusID,
                OrderStatus = Enum.GetName(typeof(OrderOrderStatus), (byte)order.OrderStatusID),
                TransactionType = (OrderTransactionType)order.TransactionType,
                ClassTypeID = order.ClassTypeID,
                DueDT = DueDT(order),
                Priority = order.Priority ?? 0,
                IsUrgent = order.IsUrgent ?? false,
                CompanyID = order.CompanyID,
                CompanyName = order.Company?.Name ?? null,
                AssignedToID = assignedToRole?.EmployeeID,
                AssignedToText = assignedToRole?.Employee?.ShortName ?? null,
                SalespersonID = salespersonRole?.EmployeeID,
                SalespersonText = salespersonRole?.Employee?.ShortName ?? null,
                LocationID = order.LocationID,
                Location = order.Location?.Name ?? null,
                Price = order.PriceTotal ?? 0m,
                BalanceOutstanding = order.PaymentBalanceDue ?? 0m,
                DaysLate = daysLate,
                ProductionDaysLate = ProductionDaysLate(order),
                Description = order.Description,
                PaymentTermsText = order.Company?.DefaultPaymentTerms?.Name ?? null,
                OrderItemCount = (short)(order.Items == null ? 0 : order.Items.Count()),
                Tags = order
                       .TagLinks?
                       .Select(y => new SimpleTag()
                       {
                           ID = y.TagID,
                           Name = y.Tag == null ? null : y.Tag.Name,
                           Color = y.Tag == null ? null : y.Tag.RGB
                       })
                       .ToList()
            };
        }

        static private DateTime? DueDT(OrderData order)
        {
            return order.Dates?.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.ProductionDue)?.KeyDT;
        }

        static private int? ProductionDaysLate(OrderData order)
        {
            int daysLate = 0;

            DateTime? dueDT = order.Dates?.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.ProductionDue)?.KeyDT;

            if (dueDT.HasValue)
            {
                daysLate = (DateTime.UtcNow - dueDT.Value).Days;
            }

            return daysLate > 0 ? daysLate : 0;
        }

        /// <summary>
        /// Gets Board Data for Orders
        /// </summary>
        /// <param name="aelOp">Board Conditions</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        private IQueryable<IBoardItem> GetOrderBoardData(AELOperation aelOp, int[] ids)
        {
            var result = ctx.OrderData
                .Include(x => x.Company)
                .Include(x => x.Location)
                .Include(x => x.Dates)
                .Include(x => x.TagLinks).ThenInclude(x => x.Tag)
                .Include(x => x.EmployeeRoles).ThenInclude(x => x.Employee)
                .Include(x => x.Items)
                .Include(x => x.Company).ThenInclude(x => x.DefaultPaymentTerms)
                .Where(new AELDataProvider(BID, ctx), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderByDescending(x => x.IsUrgent ?? false)
                .ThenBy(x => x.Priority ?? 0)
                //.ThenBy(x => DueDT(x) ?? DateTime.MinValue)
                .ThenBy(x => x.Dates.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.ProductionDue).KeyDT)
                .ThenByDescending(x => x.FormattedNumber ?? null)
                .Select(x => GetOrderBoardItem(x));
            result.ToList();
            return result;
        }

        /// <summary>
        /// Creates an EstimateBoardItem from an EstimateData
        /// </summary>
        /// <param name="estimate"></param>
        /// <returns></returns>
        private EstimateBoardItem GetEstimateBoardItem(EstimateData estimate)
        {
            #region Saleperson
            OrderEmployeeRole salespersonRole = estimate.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 1);
            #endregion Saleperson

            return new EstimateBoardItem()
            {
                ID = estimate.ID,
                ModifiedDT = estimate.ModifiedDT,
                EstimateNumber = estimate.FormattedNumber ?? null,
                OrderStatusID = (byte)estimate.OrderStatusID,
                TransactionType = (OrderTransactionType)estimate.TransactionType,
                ClassTypeID = estimate.ClassTypeID,
                DueDT = DueDT(estimate),
                Priority = estimate.Priority ?? 0,
                IsUrgent = estimate.IsUrgent ?? false,
                CompanyID = estimate.CompanyID,
                CompanyName = estimate.Company?.Name ?? null,
                SalespersonID = salespersonRole?.EmployeeID,
                Salesperson = salespersonRole?.Employee?.ShortName ?? null,
                LocationID = estimate.LocationID,
                Location = estimate.Location?.Name ?? null,
                Price = estimate.PriceTotal ?? 0m
            };
        }

        static private DateTime? DueDT(EstimateData estimate)
        {
            return estimate.Dates?.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.ProductionDue)?.KeyDate;
        }

        /// <summary>
        /// Gets Board Data for Estimates
        /// </summary>
        /// <param name="aelOp">Board Conditions</param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private IQueryable<IBoardItem> GetEstimateBoardData(AELOperation aelOp, int[] ids)
        {
            var result = ctx.EstimateData 
                .Include(x => x.Company)
                .Include(x => x.Location)
                .Include(x => x.Dates)
                .Where(new AELDataProvider(BID, ctx), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderByDescending(x => x.IsUrgent ?? false)
                .ThenBy(x => x.Priority ?? 0)
                .ThenBy(x => DueDT(x))
                .ThenByDescending(x => x.FormattedNumber ?? null)
                .Select(x => GetEstimateBoardItem(x));
            result.ToList();
            return result;
        }

        /// <summary>
        /// Creates an OrderItemBoardItem from an OrderItemData
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        private static OrderItemBoardItem GetLineItemBoardItem(OrderItemData orderItem)
        {
            #region Roles
            OrderEmployeeRole assignedToRole = orderItem.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 253);
            OrderEmployeeRole designerRole = orderItem.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 3);
            OrderEmployeeRole salespersonRole = orderItem.EmployeeRoles?.FirstOrDefault(a => a.RoleID == 1);
            #endregion Roles

            #region Order
            OrderTransactionType transactionType = OrderTransactionType.Order;
            string orderNumber = null;
            string estimateNumber = null;
            short lineItemCount = 0;
            int companyID = 0;
            string companyName = null;
            byte orderStatusID = 0;

            if (orderItem.Order != null)
            {
                transactionType = (OrderTransactionType)orderItem.Order.TransactionType;
                orderNumber = (transactionType == OrderTransactionType.Order) ? orderItem.Order.FormattedNumber : null;
                estimateNumber = (transactionType == OrderTransactionType.Estimate) ? orderItem.Order.FormattedNumber : null;
                lineItemCount = (short)(orderItem.Order.Items == null ? 0 : orderItem.Order.Items.Count());
                companyID = orderItem.Order.CompanyID;
                companyName = orderItem.Order.Company?.Name;
                orderStatusID = (byte)orderItem.Order.OrderStatusID;
            }
            #endregion Order

            return new OrderItemBoardItem()
            {
                ID = orderItem.ID,
                ClassTypeID = orderItem.ClassTypeID,
                ModifiedDT = orderItem.ModifiedDT,
                TransactionType = transactionType,
                DueDT = DueDT(orderItem),
                //RoleIDs---start
                AssignedToID = assignedToRole?.EmployeeID,
                DesignerID = designerRole?.EmployeeID,
                SalespersonID = salespersonRole?.EmployeeID,
                //RoleIDs---end
                //Role Names---start
                AssignedToText = assignedToRole?.Employee?.ShortName ?? null,
                AssignedToInitials = (assignedToRole?.Employee ?? null) != null ? assignedToRole?.Employee?.First?.Substring(0, 1) + assignedToRole?.Employee?.Last?.Substring(0, 1) : null,
                DesignerText = designerRole?.Employee?.ShortName ?? null,
                SalespersonText = salespersonRole?.Employee?.ShortName ?? null,
                //Role Names---end
                OrderNumber = orderNumber ?? null,
                EstimateNumber = estimateNumber ?? null,
                OrderID = orderItem.OrderID,
                Name = orderItem.Name ?? null,
                Description = orderItem.Description ?? null,
                LineItemName = orderItem.Name ?? null,
                LineItemIndex = orderItem.ItemNumber,
                LineItemCount = lineItemCount,
                CompanyID = companyID,
                CompanyName = companyName ?? null,
                OrderStatusID = orderStatusID,
                ItemStatusID = orderItem.ItemStatusID,
                ItemStatusText = orderItem.OrderItemStatus?.Name ?? null,
                SubStatusID = orderItem.SubStatusID,
                SubStatusText = orderItem.SubStatus?.Name ?? null,
                Priority = orderItem.Priority ?? 0,
                IsUrgent = orderItem.IsUrgent ?? false,
                Price = orderItem.PriceTotal ?? 0,
                Tags = orderItem
                       .TagLinks?
                       .Select(y => new SimpleTag()
                       {
                           ID = y.TagID,
                           Name = y.Tag == null ? null : y.Tag.Name,
                           Color = y.Tag == null ? null : y.Tag.RGB
                       })
                       .ToList()
            };
        }

        private static DateTime? DueDT(OrderItemData orderItem)
        {
            return orderItem.Dates?.FirstOrDefault(z => z.KeyDateType == OrderKeyDateType.ProductionDue)?.KeyDate;
        }

        /// <summary>
        /// Gets Board Data for Line Items
        /// </summary>
        /// <param name="aelOp">Board Conditions</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        private IQueryable<IBoardItem> GetLineItemBoardData(AELOperation aelOp, int[] ids)
        {
            var result = ctx.OrderItemData
                .Include(x => x.Order)
                .Include(x => x.Order.Company)
                .Include(x => x.Order.Items)
                .Include(x => x.SubStatus)
                .Include(x => x.OrderItemStatus)
                .Include(x => x.Dates)
                .Include(x => x.EmployeeRoles).ThenInclude(x => x.Employee)
                .Include(x => x.TagLinks).ThenInclude(x => x.Tag)
                .Where(new AELDataProvider(BID, ctx), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderByDescending(x => x.IsUrgent ?? false)
                .ThenBy(x => x.Priority ?? 0)
                .ThenBy(x => x.Order.Dates.FirstOrDefault(z => z.KeyDateType.Equals(OrderKeyDateType.ProductionDue)).KeyDate)
                .ThenByDescending(x => x.Order == null ? "" : (x.Order.FormattedNumber ?? null))
                .Select(x => GetLineItemBoardItem(x));
            result.ToList();
            return result;
        }

        /// <summary>
        /// Creates an CompanyBoardItem from an CompanyData
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        private static CompanyBoardItem GetCompanyBoardItem(CompanyData company)
        {
            XmlDocument addressXmlMetaData = new XmlDocument();
            string addressXmlMetaDataFromDB = company.CompanyLocators?.FirstOrDefault(l => l.LocatorType == 1)?.MetaDataXML;
            string addressJSON = "";
            if (!string.IsNullOrEmpty(addressXmlMetaDataFromDB))
            {
                addressXmlMetaData.LoadXml(addressXmlMetaDataFromDB);
                addressJSON = JsonConvert.SerializeXmlNode(addressXmlMetaData);
            }

            string primaryPhoneNumber = company.CompanyLocators?.FirstOrDefault(l => l.LocatorType == 2 && l.SortIndex == -1)?.Locator;

            var billingContact = new ContactData(); 
            var primaryContact = new ContactData();
            foreach(CompanyContactLink c in company.CompanyContactLinks)
            {
                if (c.IsBilling == true)
                {
                    billingContact = c.Contact;
                }
                if (c.IsPrimary == true)
                {
                    primaryContact = c.Contact;
                }
            }
            var salesPerson = company.EmployeeTeam?.EmployeeTeamLinks?.FirstOrDefault(l => l.RoleID == 1)?.Employee;

            return new CompanyBoardItem()
            {
                ID = company.ID,
                ModifiedDT = company.ModifiedDT,
                ClassTypeID = company.ClassTypeID,
                CompanyName = company.Name ?? null,
                Name = company.Name ?? null, 
                Salesperson = salesPerson?.LongName ?? null,
                PrimaryContactID = primaryContact?.ID,
                PrimaryContactText = primaryContact?.LongName ?? null,
                BillingContactID = billingContact?.ID,
                BillingContactText = billingContact?.LongName ?? null,
                LocationID = company.LocationID,
                Address = addressJSON,
                TimeZone = company.TimeZone?.Name ?? null,
                Phone = primaryPhoneNumber ?? null,
                StatusID = company.StatusID,
                PaymentTermsText = company.DefaultPaymentTerms?.Name ?? null,
            };
        }

        /// <summary>
        /// Gets Board Data for Contacts
        /// </summary>
        /// <param name="aelOp">Board Conditions</param>
        /// <param name="ids">List of IDs to return BoardData for</param>
        /// <returns></returns>
        private IQueryable<IBoardItem> GetCompanyBoardData(AELOperation aelOp, int[] ids)
        {
            var companies = ctx.CompanyData
                .Include(x => x.EmployeeTeam)
                .Include(x => x.EmployeeTeam).ThenInclude(x => x.EmployeeTeamLinks).ThenInclude(x => x.Employee)
                .Include(x => x.CompanyLocators)
                .Include(x => x.TimeZone)
                .Include(x => x.DefaultPaymentTerms)
                .Include(x => x.CompanyContactLinks)
                .Include(x => x.CompanyContactLinks).ThenInclude(x => x.Contact)
                .Where(new AELDataProvider(BID, ctx), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderBy(x => x.Name ?? null)
                .ThenBy(x => x.ID);
            companies.ToList();
            return companies.Select(x => GetCompanyBoardItem(x));
        }
        
        /// <summary>
        /// Checks if the user is authorized to view the board
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="boardDef">Board Definition object</param>
        /// <returns></returns>
        private async Task<bool> IsUserAuthorizedForBoard(int userID, BoardDefinitionData boardDef)
        {
            var isAuth = true;

            var user = await ctx.UserLink.Where(u => u.BID == BID && u.AuthUserID == userID).Select(u => new { u.EmployeeID, u.UserAccessType }).FirstOrDefaultAsync();
            if (user.UserAccessType == UserAccessType.None || user.UserAccessType == UserAccessType.Contact || user.UserAccessType == UserAccessType.ContactAdministrator)
            {
                isAuth = false;
            }

            #region END-3631 : Currently not implemented as employee roles aren't setup for this yet
            //if (boardDef.RoleLinks != null && boardDef.RoleLinks.Count > 0)
            //{
            //    var user = await ctx.UserLink.Where(u => u.BID == BID && u.AuthUserID == userID).Select(u => new { EmployeeID = u.EmployeeID, UserAccessType = u.UserAccessType }).FirstOrDefaultAsync();
            //    if (user == null)
            //    {
            //        isAuth = false;
            //    }
            //    else
            //    {
            //        if (user.UserAccessType == UserAccessType.None || user.UserAccessType == UserAccessType.Contact || user.UserAccessType == UserAccessType.ContactAdministrator)
            //        {
            //            isAuth = false;
            //        }
            //        else if (user.UserAccessType == UserAccessType.Employee)
            //        {
            //            var roleID = await ctx.OrderEmployeeRole.Where(r => r.BID == BID && r.EmployeeID == user.EmployeeID).Select(r => r.RoleID).FirstOrDefaultAsync();
            //            if (!boardDef.RoleLinks.Any(rl => rl.RoleID == roleID))
            //                isAuth = false;
            //        }
            //    }
            //}
            #endregion

            return isAuth;
        }

        #endregion

    }
}
