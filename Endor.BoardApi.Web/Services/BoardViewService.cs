﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common;
using Endor.Api.Common.Services;
using Endor.BoardApi.Web.Classes;
using Endor.BoardApi.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.BoardApi.Web.Services
{
    /// <summary>
    /// Board View Service
    /// </summary>
    public class BoardViewService : AtomCRUDService<BoardView, short>, ISimpleListableViewService<SimpleBoardView, short>
    {
        /// <summary>
        /// Board View Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper"></param>
        public BoardViewService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Property to get  SimpleCompanyData from ApiContext
        /// </summary>
        public DbSet<SimpleBoardView> SimpleListSet => ctx.SimpleBoardView;

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Gets a list of BoardView with includes
        /// </summary>
        /// <param name="ID">BoardView Id</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<BoardView> GetWithIncludes(int ID, BoardViewIncludes includes = null)
        {
            return (await this.FilteredGetAsync(t => t.ID == ID, includes)).FirstOrDefault();
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(BoardView item, IExpandIncludes includes = null)
        {
            if (!(includes is BoardViewIncludes boardViewIncludes) || boardViewIncludes.BoardDefinitionLevel != IncludesLevel.Full)
                return base.MapItem(item, includes);

            BoardView boardView = this.ctx.BoardView.IncludeAll(new[] { "BoardViewLinks", "BoardViewLinks.BoardDefinitionData" })
                .FirstOrDefault(t => t.ID == item.ID && t.BID == this.BID);

            List<BoardDefinitionData> boards = (from board in boardView?.BoardViewLinks.Select(l => l.BoardDefinitionData)
                                                select new BoardDefinitionData()
                                                {
                                                    CummRunDurationSec = board.CummRunDurationSec,
                                                    CummRunCount = board.CummRunCount,
                                                    CummCounterDT = board.CummCounterDT,
                                                    LastRunDurationSec = board.LastRunDurationSec,
                                                    LastRecordCount = board.LastRecordCount,
                                                    LastRunDT = board.LastRunDT,
                                                    ConditionSQL = board.ConditionSQL,
                                                    ConditionFx = board.ConditionFx,
                                                    IsAlwaysShown = board.IsAlwaysShown,
                                                    LimitToRoles = board.LimitToRoles,
                                                    DataType = board.DataType,
                                                    Description = board.Description,
                                                    Name = board.Name,
                                                    IsActive = board.IsActive,
                                                    IsSystem = board.IsSystem,
                                                    ModifiedDT = board.ModifiedDT,
                                                    ClassTypeID = board.ClassTypeID,
                                                    ID = board.ID,
                                                    BID = board.BID,
                                                    AverageRunDuration = board.AverageRunDuration,
                                                }).ToList();

            item.Boards = boards;

            return base.MapItem(item, includes);
        }

        /// <summary>
        /// Board View Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">ContactData</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(BoardView newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Board View Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old ContactData</param>
        /// <param name="updatedModel">Updated ContactData</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(BoardView oldModel, BoardView updatedModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// Board View Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned ContactData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(BoardView clone)
        {
            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Get Child Service Associations for BoardView
        /// </summary>
        /// <returns></returns>
        public override IChildServiceAssociation<BoardView, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<BoardView, short>[]{ };
        }

        /// <summary>
        /// Lookup BoardView by query and ID - Override
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">BoardView ID</param>
        /// <returns></returns>
        public override IQueryable<BoardView> WherePrimary(IQueryable<BoardView> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Additional logic to before before validation
        /// </summary>
        /// <param name="newModel">BoardView model</param>
        public override void DoBeforeValidate(BoardView newModel)
        {
            return;
        }

        /// <summary>
        /// Gets a list of Board View for the specified filters
        /// </summary>
        /// <param name="filters">Board View Filters</param>
        /// <returns></returns>
        public async Task<List<BoardView>> GetAllWithFilters(BoardViewFilters filters)
        {
            return await ctx.Set<BoardView>().IncludeAll(GetIncludes())
                .Where(bdd => bdd.BID == BID)
                .WhereAll(filters.WherePredicates())
                .ToListAsync();
        }

        /// <summary>
        /// Links a board to a board view
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="BoardId"></param>
        /// <param name="sortIndex"></param>
        /// <returns></returns>
        public async Task<(bool, string)> LinkBoard(short ID, short BoardId, byte? sortIndex)
        {
            try
            {
                SqlParameter sortindexParam;

                if (sortIndex.HasValue)
                    sortindexParam = new SqlParameter("@sortindex", sortIndex);
                else
                    sortindexParam = new SqlParameter("@sortindex", DBNull.Value);

                object[] myParams = {
                new SqlParameter("@bid", this.BID),
                new SqlParameter("@boardid", BoardId),
                new SqlParameter("@boardviewid", ID),
                sortindexParam
            };

                await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[Board.View.Action.Link] @bid, @boardid, @boardviewid, @sortindex;", parameters: myParams);
                return (true, null);
            }
            catch (Exception ex)
            {
                return (false, ex.Message);
            }
        }

        /// <summary>
        /// Unlinks a board to a board view
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="BoardId"></param>
        /// <returns></returns>
        public async Task<(bool, string)> UnlinkBoard(short ID, short BoardId)
        {
            try
            {
                object[] myParams = {
                new SqlParameter("@bid", this.BID),
                new SqlParameter("@boardid", BoardId),
                new SqlParameter("@boardviewid", ID)
            };

                await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[Board.View.Action.Unlink] @bid, @boardid, @boardviewid;", parameters: myParams);
                return (true, null);
            }
            catch (Exception ex)
            {
                return (false, ex.Message);
            }
        }
    }
}
