﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;

namespace Endor.BoardApi.Web.Services
{
    /// <summary>
    /// Service for board view links
    /// </summary>
    public class BoardViewLinkService : LinkCRUDService<BoardViewLink>, ICloneableChildCRUDService<BoardDefinitionData, BoardViewLink>
    {
        private readonly ITaskQueuer _taskQueuer;
        private readonly ITenantDataCache _cache;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper"></param>
        public BoardViewLinkService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, bid, rtmClient, taskQueuer, migrationHelper)
        {
            _taskQueuer = taskQueuer;
            _cache = cache;
        }

        /// <summary>
        /// Get old model expanding BoardView
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<BoardViewLink> GetOldModelAsync(BoardViewLink newModel)
        {
            var boardLink = this.ctx.BoardViewLink.IncludeAll(new []{ "BoardView" }).FirstOrDefault(t => t.BID == newModel.BID && t.BoardID == newModel.BoardID);
            return await Task.FromResult(boardLink);
        }

        /// <summary>
        /// Do before clone
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(BoardDefinitionData parentModel, BoardViewLink childModel)
        {
            return Task.FromResult<OrderOrderLink>(null);
        }

        /// <summary>
        /// Do before update
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(BoardViewLink oldModel, BoardViewLink newModel)
        {
            if (newModel.BID == 0)
                newModel.BID = BID;

            if (newModel?.BoardView != null)
            {
                var childSvc = new BoardViewService(this.ctx, BID, _taskQueuer, _cache, logger, rtmClient, migrationHelper);
                if (newModel.BoardView.BID == 0)
                    newModel.BoardView.BID = this.BID;
                await childSvc.DoBeforeUpdateAsync(await childSvc.GetOldModelAsync(newModel.BoardView), newModel.BoardView);
            }
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// called on do before create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(BoardViewLink newModel, Guid? tempGuid = null)
        {
            if (newModel?.BoardView != null)
            {
                var childSvc = new BoardViewService(this.ctx, BID, _taskQueuer, _cache, logger, rtmClient, migrationHelper);
                newModel.BoardView.BID = this.BID;
                await childSvc.DoBeforeCreateAsync(newModel.BoardView);
                newModel.ViewID = newModel.BoardView.ID;
            }
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
    }
}
